<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>DMS</title>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
	<script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>
      <script>
         WebFont.load({
                  google: {"families":["Montserrat:300,400,500,600,700","Roboto:300,400,500,600,700"]},
                  active: function() {
                      sessionStorage.fonts = true;
                  }
                });
              
      </script>
      <!--end::Web font -->
      <!--begin::Global Theme Styles -->
      <link href=" {{ asset('assets/admin/assets/vendors/base/vendors.bundle.css') }}" rel="stylesheet" type="text/css" />
      <!--RTL version:<link href="assets/vendors/base/vendors.bundle.rtl.css" rel="stylesheet" type="text/css" />-->
      <link href="{{ asset('assets/admin/assets/demo/demo3/base/style.bundle.css') }}" rel="stylesheet" type="text/css" />
      <!--RTL version:<link href="assets/demo/demo3/base/style.bundle.rtl.css" rel="stylesheet" type="text/css" />-->
      <!--end::Global Theme Styles -->
      <!--begin::Page Vendors Styles -->
      <link href="{{ asset('assets/admin/assets/vendors/custom/fullcalendar/fullcalendar.bundle.css" rel="stylesheet') }}" type="text/css" />
      <!--RTL version:<link href="assets/vendors/custom/fullcalendar/fullcalendar.bundle.rtl.css" rel="stylesheet" type="text/css" />-->
      <!--end::Page Vendors Styles -->
      <link rel="shortcut icon" href="{{ asset('assets/admin/assets/demo/demo3/media/img/logo/favicon.ico') }}" />
       
       <!--<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">-->
   
</head>
<body>
    <div id="app">
        <nav class="navbar navbar-default navbar-static-top">
            <div class="container">
                

                <div class="collapse navbar-collapse" id="app-navbar-collapse">
                    <!-- Left Side Of Navbar -->
                    <ul class="nav navbar-nav">
                        &nbsp;
                    </ul>

                </div>
            </div>
        </nav>

        @yield('content')
    </div>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>
</body>
</html>
