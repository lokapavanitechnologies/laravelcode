<html lang="{{ app()->getLocale() }}">
   <!-- begin::Head -->
   <head>
      <meta charset="utf-8" />
      <title>DMS</title>
      <meta name="description" content="Latest updates and statistic charts">
      <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no">
	  <meta name="csrf-token" content="{{ csrf_token() }}">
      <!--begin::Web font -->
      <script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>
	  <script src="assets/admin/assets/app/js/commonvalidation.js"></script>
      <script>
         WebFont.load({
                  google: {"families":["Montserrat:300,400,500,600,700","Roboto:300,400,500,600,700"]},
                  active: function() {
                      sessionStorage.fonts = true;
                  }
                });
              
      </script>
      <!--end::Web font -->
      <!--begin::Global Theme Styles -->
      <link href=" {{ asset('assets/admin/assets/vendors/base/vendors.bundle.css') }}" rel="stylesheet" type="text/css" />
      <!--RTL version:<link href="assets/vendors/base/vendors.bundle.rtl.css" rel="stylesheet" type="text/css" />-->
      <link href="{{ asset('assets/admin/assets/demo/demo3/base/style.bundle.css') }}" rel="stylesheet" type="text/css" />
      <!--RTL version:<link href="assets/demo/demo3/base/style.bundle.rtl.css" rel="stylesheet" type="text/css" />-->
      <!--end::Global Theme Styles -->
      <!--begin::Page Vendors Styles -->
      <link href="{{ asset('assets/admin/assets/vendors/custom/fullcalendar/fullcalendar.bundle.css" rel="stylesheet') }}" type="text/css" />
      <!--RTL version:<link href="assets/vendors/custom/fullcalendar/fullcalendar.bundle.rtl.css" rel="stylesheet" type="text/css" />-->
      <!--end::Page Vendors Styles -->
      <link rel="shortcut icon" href="{{ asset('assets/admin/assets/demo/demo3/media/img/logo/favicon.ico') }}" />
       
       <!--<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">-->
   </head>