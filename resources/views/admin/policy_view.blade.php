<!DOCTYPE html>
<html lang="en">

	<!-- begin::Head -->
	<head>
		<meta charset="utf-8" />
		<title>DMS</title>
		<meta name="description" content="Latest updates and statistic charts">
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no">

		<!--begin::Web font -->
		<script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
		<!--<script src="assets/app/js/editor.js"></script>-->
		
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
		<script>
			WebFont.load({
            google: {"families":["Montserrat:300,400,500,600,700","Roboto:300,400,500,600,700"]},
            active: function() {
                sessionStorage.fonts = true;
            }
          });
        </script>

		<!--end::Web font -->

		<!--begin::Global Theme Styles -->
		<link href="assets/admin/assets/vendors/base/vendors.bundle.css" rel="stylesheet" type="text/css" />
		<link href="assets/admin/assets/vendors/base/style.css" rel="stylesheet" type="text/css" />
		<link href="assets/admin/assets/vendors/base/style2.css" rel="stylesheet" type="text/css" />
        <link href="assets/admin/assets/demo/demo3/base/style.bundle.css" rel="stylesheet" type="text/css" />
        <link href="assets/admin/assets/demo/demo3/base/style.bundle.css" rel="stylesheet" type="text/css" />
        <link href="assets/admin/assets/demo/demo3/base/summernote.css" rel="stylesheet">
        <!--<link href="assets/demo/demo3/base/editor.css" rel="stylesheet" type="text/css" />-->

		<!--RTL version:<link href="assets/vendors/base/vendors.bundle.rtl.css" rel="stylesheet" type="text/css" />-->
		

		<!--RTL version:<link href="assets/demo/demo3/base/style.bundle.rtl.css" rel="stylesheet" type="text/css" />-->
          <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">
        
           <link href="https://www.webprojectbuilder.com/assets/plugins/themify-icons/themify-icons.min.css">
        
          <link rel="stylesheet" href="https://www.webprojectbuilder.com/assets/css/bootstrap-iconpicker.css">
        <link href="https://www.webprojectbuilder.com/assets/css/themify-icons/themify-icons.css" rel="stylesheet" media="screen">

		<!--end::Global Theme Styles -->
		<link rel="shortcut icon" href="assets/admin/assets/demo/demo3/media/img/logo/favicon.ico" />
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css">
        
<!--        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">-->
        
      <!--<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">-->
       
        <style>
        .m-section .m-section__content {
        color: black;
    }

    .m-portlet .m-portlet__body {
        color: black;
    }

    .m-body .m-content {
        padding: 30px 25px;
    }
        </style>
	</head>

	<!-- end::Head -->

	<!-- begin::Body -->
	<body class="m-page--fluid m--skin- m-content--skin-light2 m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default">

		<!-- begin:: Page -->
		<div class="m-grid m-grid--hor m-grid--root m-page">

			<!-- BEGIN: Header -->
		@include('layout.header')


			<!-- END: Header -->

			<!-- begin::Body -->
			<div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">

				<!-- BEGIN: Left Aside -->
				<button class="m-aside-left-close m-aside-left-close--skin-dark" id="m_aside_left_close_btn"><i class="la la-close"></i></button>
				<div id="m_aside_left" class="m-grid__item	m-aside-left  m-aside-left--skin-dark ">

					<!-- BEGIN: Aside Menu -->
					@include('layout.sidemenu')

					<!-- END: Aside Menu -->
				</div>

				<!-- END: Left Aside -->
				<div class="m-grid__item m-grid__item--fluid m-wrapper">

					<!-- BEGIN: Subheader -->
					<div class="m-subheader ">
						<div class="d-flex align-items-center">
							<div class="mr-auto">
								<h3 class="m-subheader__title m-subheader__title--separator">Policy</h3>
								<ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
									<li class="m-nav__item m-nav__item--home">
										<a href="#" class="m-nav__link m-nav__link--icon">
											<i class="m-nav__link-icon la la-home"></i>
										</a>
									</li>
									<li class="m-nav__separator">-</li>
									<li class="m-nav__item">
										<a href="" class="m-nav__link">
											<span class="m-nav__link-text">Actions</span>
										</a>
									</li>
									<!--<li class="m-nav__separator">-</li>
									<li class="m-nav__item">
										<a href="" class="m-nav__link">
											<span class="m-nav__link-text">Create New Post</span>
										</a>
									</li>-->
								</ul>
							</div>
							<div>
								<!--<div class="m-dropdown m-dropdown--inline m-dropdown--arrow m-dropdown--align-right m-dropdown--align-push" m-dropdown-toggle="hover" aria-expanded="true">
									<a href="#" class="m-portlet__nav-link btn btn-lg btn-secondary  m-btn m-btn--outline-2x m-btn--air m-btn--icon m-btn--icon-only m-btn--pill  m-dropdown__toggle">
										<i class="la la-plus m--hide"></i>
										<i class="la la-ellipsis-h"></i>
									</a>
									<div class="m-dropdown__wrapper">
										<span class="m-dropdown__arrow m-dropdown__arrow--right m-dropdown__arrow--adjust"></span>
										<div class="m-dropdown__inner">
											<div class="m-dropdown__body">
												<div class="m-dropdown__content">
													<ul class="m-nav">
														<li class="m-nav__section m-nav__section--first m--hide">
															<span class="m-nav__section-text">Quick Actions</span>
														</li>
														<li class="m-nav__item">
															<a href="" class="m-nav__link">
																<i class="m-nav__link-icon flaticon-share"></i>
																<span class="m-nav__link-text">Activity</span>
															</a>
														</li>
														<li class="m-nav__item">
															<a href="" class="m-nav__link">
																<i class="m-nav__link-icon flaticon-chat-1"></i>
																<span class="m-nav__link-text">Messages</span>
															</a>
														</li>
														<li class="m-nav__item">
															<a href="" class="m-nav__link">
																<i class="m-nav__link-icon flaticon-info"></i>
																<span class="m-nav__link-text">FAQ</span>
															</a>
														</li>
														<li class="m-nav__item">
															<a href="" class="m-nav__link">
																<i class="m-nav__link-icon flaticon-lifebuoy"></i>
																<span class="m-nav__link-text">Support</span>
															</a>
														</li>
														<li class="m-nav__separator m-nav__separator--fit">
														</li>
														<li class="m-nav__item">
															<a href="#" class="btn btn-outline-danger m-btn m-btn--pill m-btn--wide btn-sm">Submit</a>
														</li>
													</ul>
												</div>
											</div>
										</div>
									</div>
								</div>-->
							</div>
						</div>
					</div>

					<!-- END: Subheader -->
	                <!--Begin::Main Portlet-->
<div class="m-content">
    <div class="row m-portlet ">
        <div class="col-xl-9">
            <!--Begin::Main Portlet-->
            <div class="">
                <!--begin: Portlet Head-->
                <div class="m-portlet__head">
                    <div class="m-portlet__head-caption">
                        <div class="m-portlet__head-title">
                            <h2 class="m-portlet__head-text"><strong>Core Standard 1: &nbsp;</strong>
                            </h2>

                        </div>
                    </div>
                    <div class="m-portlet__head-tools">
                        <ul class="m-portlet__nav">
                            <li class="m-portlet__nav-item">
                                <a href="#" data-toggle="m-tooltip" class="m-portlet__nav-link m-portlet__nav-link--icon" data-direction="left" data-width="auto" title="" data-original-title="Get help with filling up this form">
                                    <i class="flaticon-info m--icon-font-size-lg3"></i>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
                <!--end: Portlet Head-->

                <!--begin: Form Wizard-->
                <div class="m-wizard m-wizard--1 m-wizard--success m-wizard--step-first" id="m_wizard">

                    <!--begin: Message container -->
                    <div class="m-portlet__padding-x">
                        <!-- Here you can put a message or alert -->
                    </div>
                    <!--end: Message container -->

                    <!--begin: Form Wizard Head -->

                    <!--end: Form Wizard Head -->

                    <!--begin: Form Wizard Form-->
                    <div class="m-portlet__body">
                        <div class="m-section">
                            <div class="m-pricing-table-1__items row">
                                <div class=" m-pricing-table-1__item col-lg-5">
                                    <h2 class="m--font-primary"><em>Communication and patient participation </em></h2>

                                    <div class="m-separator m-separator--dashed m-separator--md"></div>
                                    <div class="m-demo__preview">
                                        <h4>
                                            Our practice provides timely and accurate communications that are patient-centric
                                        </h4>
                                    </div>
                                    <div class="m-separator m-separator--dashed m-separator--md"></div>

                                </div>
                                <div class=" m-pricing-table-1__item  col-lg-7">
                                    <div>Communication with patients includes:</div>
                                    <ul>
                                        <li>communication that occurs before the consultation, during the consultation and after the consultation</li>
                                        <li>verbal and written communication, and the use of interpreters, including sign language interpreters</li>
                                        <li>communication between the patient and</li>
                                        <ul>
                                            <li>the practitioner</li>
                                            <li>the practice team</li>
                                            <li>other clinicians in the practice.</li>
                                        </ul>
                                    </ul>
                                    <p>Communication must be patient‐centred. This means that the practice team considers the patient’s values, needs and preferences, and gives the patient time to provide input and participate actively in decisions
                                        regarding their healthcare.1
                                        Patients must be provided with the appropriate information they need to manage their condition.</p>
                                    <div>The practice must also consider the communication needs of carers and other relevant parties.
                                        <br>
                                    </div>
                                </div>
                            </div>

                        </div>

                        <div class="row">
                            <div class="col-lg-12">
                                <h4 class="m--font-brand  ">Criterion C1.1 – Information about your practice</h4>
                                <div class="m-demo" data-code-preview="true" data-code-html="true" data-code-js="false">
                                    <div class="m-demo__preview bg-primary m--font-light">
                                        <h5>Indicator</h5>
                                        <p><strong>C1.1 &gt; A&nbsp;</strong>Our patients can access up-to-date information about the practice.</p>
                                        <p>At a minimum, this information contains:</p>
                                        <ul>
                                            <li>our practice’s address and telephone numbers</li>
                                            <li>our consulting hours and details of arrangements for care outside normal opening&nbsp;</li>
                                            <li>our practice’s billing&nbsp;</li>
                                            <li>a list of our&nbsp;</li>
                                            <li>our practice’s communication policy, including when and how we receive and return telephone calls and electronic&nbsp;</li>
                                            <li>our practice’s policy for managing patient health information (or its principles and how full details can be obtained from the&nbsp;</li>
                                            <li>&nbsp;how to provide feedback or make a complaint to the practice</li>
                                            <li>details on the range of services we provide.</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="m-portlet__body">
                                    <div class="m-section">
                                        <h2 class="m-section__heading m--font-primary">Why is this important</h2>
                                        <div class="m-section__content">
                                            <p>
                                                Information about the practice, including the range and cost of services provided by the practice, is important to all patients.
                                            </p>
                                        </div>
                                    </div>
                                    <div class="m-section">
                                        <h2 class="m-section__heading m--font-primary">Meeting this Criterion</h2>
                                        <div class="m-section__content">
                                            <h6><strong>The format of the information </strong></h6>
                                            <p>
                                                You can provide this information in many formats, such as printed information sheets and text on the practice’s website. Pictures and simple language versions help patients who would otherwise be
                                                unable to read or understand the information. The practice needs to update this information regularly so that it remains accurate. Ideally, the information is updated as soon as it changes.
                                            </p>
                                            <p>
                                                If your practice serves specific ethnic communities, provide access to written information in the languages most commonly used by your patients. You could also display the languages spoken by the
                                                practice team on an information sheet or on your website.
                                            </p>
                                        </div>
                                        <div class="m-section__content">
                                            <h6><strong>Advertisements in our practice information </strong></h6>
                                            <p>
                                                If any of the material providing information about your practice contains local advertisements, include a disclaimer that states that the inclusion of advertisements is not an endorsement by the
                                                practice of those advertised services or products.
                                            </p>
                                        </div>
                                    </div>
                                    <div class="m-section">
                                        <h2 class="m-section__heading m--font-primary">Meeting each Indicator</h2>
                                        <p>C1.1 A Our patients can access up-to-date information about the practice.</p>

                                        <p>You must:</p>

                                        <ul>
                                            <li>make practice information available to patients</li>
                                            <li>update practice information if there are any changes.</li>
                                        </ul>

                                        <p>You could:</p>

                                        <ul>
                                            <li>create and maintain an up-to-date information sheet that contains all the required information in language that is clear and easily understood</li>
                                            <li>create and maintain an up-to-date website that contains all the required information about the practice in clear, simple language</li>
                                            <li>provide alternative ways to make the information available to patients who have low literacy levels (eg provide versions in languages other than English, and versions including pictures)</li>
                                            <li>provide brochures and/or signs in the waiting room, written in English and languages other than English, explaining

                                                <ul>
                                                    <li>the practice’s policy regarding its collection, storage, use, and disclosure of personal and health information</li>
                                                    <li>the practice’s fees</li>
                                                    <li>available services</li>
                                                    <li>after-hours services</li>
                                                </ul>
                                            </li>
                                            <li>display a list of names of the practice’s team members on duty</li>
                                            <li>make contact details of interpreters available</li>
                                            <li>train practice team members so that they can use the interpreter service.</li>
                                        </ul>

                                    </div>
                                </div>



                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <h4 class="m--font-brand  ">Criterion C1.2 – Telephone and electronic communications</h4>
                                <div class="m-demo" data-code-preview="true" data-code-html="true" data-code-js="false">
                                    <div class="m-demo__preview bg-primary m--font-light">
                                        <h5>Indicator</h5>
                                        <p><strong>C1.2 &gt; A&nbsp;</strong>Our patients can access up-to-date information about the practice.</p>
                                        <p>At a minimum, this information contains:</p>
                                        <ul>
                                            <li>our practice’s address and telephone numbers</li>
                                            <li>our consulting hours and details of arrangements for care outside normal opening&nbsp;</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="m-portlet__body">
                                    <div class="m-section">
                                        <h2 class="m-section__heading m--font-primary">Why is this important</h2>
                                        <div class="m-section__content">
                                            <p>
                                                Information about the practice, including the range and cost of services provided by the practice, is important to all patients.
                                            </p>
                                        </div>
                                    </div>
                                    <div class="m-section">
                                        <h2 class="m-section__heading m--font-primary">Meeting this Criterion</h2>
                                        <div class="m-section__content">
                                            <h6><strong>The format of the information </strong></h6>
                                            <p>
                                                You can provide this information in many formats, such as printed information sheets and text on the practice’s website. Pictures and simple language versions help patients who would otherwise be
                                                unable to read or understand the information. The practice needs to update this information regularly so that it remains accurate. Ideally, the information is updated as soon as it changes.
                                            </p>
                                            <p>
                                                If your practice serves specific ethnic communities, provide access to written information in the languages most commonly used by your patients. You could also display the languages spoken by the
                                                practice team on an information sheet or on your website.
                                            </p>
                                        </div>
                                        <div class="m-section__content">
                                            <h6><strong>Advertisements in our practice information </strong></h6>
                                            <p>
                                                If any of the material providing information about your practice contains local advertisements, include a disclaimer that states that the inclusion of advertisements is not an endorsement by the
                                                practice of those advertised services or products.
                                            </p>
                                        </div>
                                    </div>
                                    <div class="m-section">
                                        <h2 class="m-section__heading m--font-primary">Meeting each Indicator</h2>
                                        <p>C1.2 A Our patients can access up-to-date information about the practice.</p>

                                        <p>You must:</p>

                                        <ul>
                                            <li>make practice information available to patients</li>
                                            <li>update practice information if there are any changes.</li>
                                        </ul>

                                        <p>You could:</p>

                                        <ul>
                                            <li>create and maintain an up-to-date information sheet that contains all the required information in language that is clear and easily understood</li>
                                            <li>create and maintain an up-to-date website that contains all the required information about the practice in clear, simple language</li>
                                            <li>provide alternative ways to make the information available to patients who have low literacy levels (eg provide versions in languages other than English, and versions including pictures)</li>
                                            <li>provide brochures and/or signs in the waiting room, written in English and languages other than English, explaining

                                                <ul>
                                                    <li>the practice’s policy regarding its collection, storage, use, and disclosure of personal and health information</li>
                                                    <li>the practice’s fees</li>
                                                    <li>available services</li>
                                                    <li>after-hours services</li>
                                                </ul>
                                            </li>
                                            <li>display a list of names of the practice’s team members on duty</li>
                                            <li>make contact details of interpreters available</li>
                                            <li>train practice team members so that they can use the interpreter service.</li>
                                        </ul>

                                    </div>
                                </div>



                            </div>
                        </div>
                    </div>
                    <!--end: Form Wizard Form-->
                </div>
                <!--end: Form Wizard-->
            </div>
            <!--End::Main Portlet-->
        </div>
        <div class="col-xl-3" style="
">
            <div class="" style="
    position: fixed;
">
                <div class="m-wizard m-wizard--1 m-wizard--success m-wizard--step-first">
                    <div class="" data-tabs="true" data-tabs-contents="#m_sections">
                        <ul class="m-nav m-nav--active-bg m-nav--active-bg-padding-lg m-nav--font-lg m-nav--font-bold m--margin-bottom-20 m--margin-top-10 m--margin-right-40" id="m_nav" role="tablist">
                            <li class="m-nav__item">
                                <a class="m-nav__link" role="tab" id="m_nav_link_1" data-toggle="collapse" href="#m_nav_sub_1" aria-expanded="true">
                                    <span class="m-nav__link-title">
                                        <span class="m-nav__link-wrap">
                                            <span class="m-nav__link-text">Core Module</span>
                                        </span>
                                    </span>
                                    <span class="m-nav__link-arrow"></span>
                                </a>
                                <ul class="m-nav__sub collapse show" id="m_nav_sub_1" role="tabpanel" aria-labelledby="m_nav_link_1" data-parent="#m_nav" style="">
                                    <li class="m-nav__item">
                                        <a class="m-nav__link m-tabs__item m-tabs__item--active" data-tab-target="#m_section_3" href="#">
                                            <span class="m-nav__link-bullet m-nav__link-bullet--dot"><span></span></span>
                                            <span class="m-nav__link-text">CS1 - Communication and patient participation</span>
                                        </a>
                                    </li>
                                    <li class="m-nav__item">
                                        <a class="m-nav__link m-tabs__item" data-tab-target="#m_section_4" href="#">
                                            <span class="m-nav__link-bullet m-nav__link-bullet--dot"><span></span></span>
                                            <span class="m-nav__link-title">
                                                <span class="m-nav__link-wrap">
                                                    <span class="m-nav__link-text">CS2 - Rights and needs of patients</span>
                                                </span>
                                            </span>
                                        </a>
                                    </li>
                                    <li class="m-nav__item">
                                        <a class="m-nav__link m-tabs__item" data-tab-target="#m_section_5" href="#">
                                            <span class="m-nav__link-bullet m-nav__link-bullet--dot"><span></span></span>
                                            <span class="m-nav__link-text">CS3 - Practice governance and management</span>
                                        </a>
                                    </li>
                                    <li class="m-nav__item">
                                        <a class="m-nav__link m-tabs__item" data-tab-target="#m_section_5" href="#">
                                            <span class="m-nav__link-bullet m-nav__link-bullet--dot"><span></span></span>
                                            <span class="m-nav__link-text">CS4 - Health promotion and preventive activities</span>
                                        </a>
                                    </li>
                                    <li class="m-nav__item">
                                        <a class="m-nav__link m-tabs__item" data-tab-target="#m_section_5" href="#">
                                            <span class="m-nav__link-bullet m-nav__link-bullet--dot"><span></span></span>
                                            <span class="m-nav__link-text">CS5 - Clinical management of health issues</span>
                                        </a>
                                    </li>
                                    <li class="m-nav__item">
                                        <a class="m-nav__link m-tabs__item" data-tab-target="#m_section_5" href="#">
                                            <span class="m-nav__link-bullet m-nav__link-bullet--dot"><span></span></span>
                                            <span class="m-nav__link-text">CS6 - Information Management</span>
                                        </a>
                                    </li>
                                    <li class="m-nav__item">
                                        <a class="m-nav__link m-tabs__item" data-tab-target="#m_section_5" href="#">
                                            <span class="m-nav__link-bullet m-nav__link-bullet--dot"><span></span></span>
                                            <span class="m-nav__link-text">CS7 - Content of patient health records</span>
                                        </a>
                                    </li>
                                </ul>
                            </li>

                            <li class="m-nav__item">
                                <a class="m-nav__link collapsed" role="tab" id="m_nav_link_2" data-toggle="collapse" href="#m_nav_sub_2" aria-expanded="false">
                                    <span class="m-nav__link-title">
                                        <span class="m-nav__link-wrap">
                                            <span class="m-nav__link-text">Quality Improvement Module</span>
                                        </span>
                                    </span>
                                    <span class="m-nav__link-arrow"></span>
                                </a>
                                <ul class="m-nav__sub collapse" id="m_nav_sub_2" role="tabpanel" aria-labelledby="m_nav_link_2" data-parent="#m_nav" style="">
                                    <li class="m-nav__item">
                                        <a class="m-nav__link" data-toggle="tab" href="#m_section_7" role="tab">
                                            <span class="m-nav__link-bullet m-nav__link-bullet--line"><span></span></span>
                                            <span class="m-nav__link-text">QIS 1: Quality Improvement</span>
                                        </a>
                                    </li>
                                    <li class="m-nav__item">
                                        <a class="m-nav__link" data-toggle="tab" href="#m_section_8" role="tab">
                                            <span class="m-nav__link-bullet m-nav__link-bullet--line"><span></span></span>
                                            <span class="m-nav__link-title">
                                                <span class="m-nav__link-wrap">
                                                    <span class="m-nav__link-text">QIS 2: Clinical indicators</span>
                                                </span>
                                            </span>
                                        </a>
                                    </li>
                                    <li class="m-nav__item">
                                        <a class="m-nav__link" data-toggle="tab" href="#m_section_9" role="tab">
                                            <span class="m-nav__link-bullet m-nav__link-bullet--line"><span></span></span>
                                            <span class="m-nav__link-text">QIS 3: Clinical risk management</span>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>


</div>
                    
				</div>
			</div>

			<!-- end:: Body -->

			<!-- begin::Footer -->
			@include('layout.footer')

			<!-- end::Footer -->
		</div>

		<!-- end:: Page -->

		<!-- begin::Quick Sidebar -->
		<div id="m_quick_sidebar" class="m-quick-sidebar m-quick-sidebar--tabbed m-quick-sidebar--skin-light">
			<div class="m-quick-sidebar__content m--hide">
				<span id="m_quick_sidebar_close" class="m-quick-sidebar__close"><i class="la la-close"></i></span>
				<ul id="m_quick_sidebar_tabs" class="nav nav-tabs m-tabs m-tabs-line m-tabs-line--brand" role="tablist">
					<li class="nav-item m-tabs__item">
						<a class="nav-link m-tabs__link active" data-toggle="tab" href="#m_quick_sidebar_tabs_messenger" role="tab">Messages</a>
					</li>
					<li class="nav-item m-tabs__item">
						<a class="nav-link m-tabs__link" data-toggle="tab" href="#m_quick_sidebar_tabs_settings" role="tab">Settings</a>
					</li>
					<li class="nav-item m-tabs__item">
						<a class="nav-link m-tabs__link" data-toggle="tab" href="#m_quick_sidebar_tabs_logs" role="tab">Logs</a>
					</li>
				</ul>
				<div class="tab-content">
					<div class="tab-pane active" id="m_quick_sidebar_tabs_messenger" role="tabpanel">
						<div class="m-messenger m-messenger--message-arrow m-messenger--skin-light">
							<div class="m-messenger__messages m-scrollable">
								<div class="m-messenger__wrapper">
									<div class="m-messenger__message m-messenger__message--in">
										<div class="m-messenger__message-pic">
											<img src="assets/app/media/img//users/user3.jpg" alt="" />
										</div>
										<div class="m-messenger__message-body">
											<div class="m-messenger__message-arrow"></div>
											<div class="m-messenger__message-content">
												<div class="m-messenger__message-username">
													Megan wrote
												</div>
												<div class="m-messenger__message-text">
													Hi Bob. What time will be the meeting ?
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="m-messenger__wrapper">
									<div class="m-messenger__message m-messenger__message--out">
										<div class="m-messenger__message-body">
											<div class="m-messenger__message-arrow"></div>
											<div class="m-messenger__message-content">
												<div class="m-messenger__message-text">
													Hi Megan. It's at 2.30PM
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="m-messenger__wrapper">
									<div class="m-messenger__message m-messenger__message--in">
										<div class="m-messenger__message-pic">
											<img src="assets/app/media/img//users/user3.jpg" alt="" />
										</div>
										<div class="m-messenger__message-body">
											<div class="m-messenger__message-arrow"></div>
											<div class="m-messenger__message-content">
												<div class="m-messenger__message-username">
													Megan wrote
												</div>
												<div class="m-messenger__message-text">
													Will the development team be joining ?
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="m-messenger__wrapper">
									<div class="m-messenger__message m-messenger__message--out">
										<div class="m-messenger__message-body">
											<div class="m-messenger__message-arrow"></div>
											<div class="m-messenger__message-content">
												<div class="m-messenger__message-text">
													Yes sure. I invited them as well
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="m-messenger__datetime">2:30PM</div>
								<div class="m-messenger__wrapper">
									<div class="m-messenger__message m-messenger__message--in">
										<div class="m-messenger__message-pic">
											<img src="assets/app/media/img//users/user3.jpg" alt="" />
										</div>
										<div class="m-messenger__message-body">
											<div class="m-messenger__message-arrow"></div>
											<div class="m-messenger__message-content">
												<div class="m-messenger__message-username">
													Megan wrote
												</div>
												<div class="m-messenger__message-text">
													Noted. For the Coca-Cola Mobile App project as well ?
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="m-messenger__wrapper">
									<div class="m-messenger__message m-messenger__message--out">
										<div class="m-messenger__message-body">
											<div class="m-messenger__message-arrow"></div>
											<div class="m-messenger__message-content">
												<div class="m-messenger__message-text">
													Yes, sure.
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="m-messenger__wrapper">
									<div class="m-messenger__message m-messenger__message--out">
										<div class="m-messenger__message-body">
											<div class="m-messenger__message-arrow"></div>
											<div class="m-messenger__message-content">
												<div class="m-messenger__message-text">
													Please also prepare the quotation for the Loop CRM project as well.
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="m-messenger__datetime">3:15PM</div>
								<div class="m-messenger__wrapper">
									<div class="m-messenger__message m-messenger__message--in">
										<div class="m-messenger__message-no-pic m--bg-fill-danger">
											<span>M</span>
										</div>
										<div class="m-messenger__message-body">
											<div class="m-messenger__message-arrow"></div>
											<div class="m-messenger__message-content">
												<div class="m-messenger__message-username">
													Megan wrote
												</div>
												<div class="m-messenger__message-text">
													Noted. I will prepare it.
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="m-messenger__wrapper">
									<div class="m-messenger__message m-messenger__message--out">
										<div class="m-messenger__message-body">
											<div class="m-messenger__message-arrow"></div>
											<div class="m-messenger__message-content">
												<div class="m-messenger__message-text">
													Thanks Megan. I will see you later.
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="m-messenger__wrapper">
									<div class="m-messenger__message m-messenger__message--in">
										<div class="m-messenger__message-pic">
											<img src="assets/admin/assets/app/media/img//users/user3.jpg" alt="" />
										</div>
										<div class="m-messenger__message-body">
											<div class="m-messenger__message-arrow"></div>
											<div class="m-messenger__message-content">
												<div class="m-messenger__message-username">
													Megan wrote
												</div>
												<div class="m-messenger__message-text">
													Sure. See you in the meeting soon.
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="m-messenger__seperator"></div>
							<div class="m-messenger__form">
								<div class="m-messenger__form-controls">
									<input type="text" name="" placeholder="Type here..." class="m-messenger__form-input">
								</div>
								<div class="m-messenger__form-tools">
									<a href="" class="m-messenger__form-attachment">
										<i class="la la-paperclip"></i>
									</a>
								</div>
							</div>
						</div>
					</div>
					<div class="tab-pane" id="m_quick_sidebar_tabs_settings" role="tabpanel">
						<div class="m-list-settings m-scrollable">
							<div class="m-list-settings__group">
								<div class="m-list-settings__heading">General Settings</div>
								<div class="m-list-settings__item">
									<span class="m-list-settings__item-label">Email Notifications</span>
									<span class="m-list-settings__item-control">
										<span class="m-switch m-switch--outline m-switch--icon-check m-switch--brand">
											<label>
												<input type="checkbox" checked="checked" name="">
												<span></span>
											</label>
										</span>
									</span>
								</div>
								<div class="m-list-settings__item">
									<span class="m-list-settings__item-label">Site Tracking</span>
									<span class="m-list-settings__item-control">
										<span class="m-switch m-switch--outline m-switch--icon-check m-switch--brand">
											<label>
												<input type="checkbox" name="">
												<span></span>
											</label>
										</span>
									</span>
								</div>
								<div class="m-list-settings__item">
									<span class="m-list-settings__item-label">SMS Alerts</span>
									<span class="m-list-settings__item-control">
										<span class="m-switch m-switch--outline m-switch--icon-check m-switch--brand">
											<label>
												<input type="checkbox" name="">
												<span></span>
											</label>
										</span>
									</span>
								</div>
								<div class="m-list-settings__item">
									<span class="m-list-settings__item-label">Backup Storage</span>
									<span class="m-list-settings__item-control">
										<span class="m-switch m-switch--outline m-switch--icon-check m-switch--brand">
											<label>
												<input type="checkbox" name="">
												<span></span>
											</label>
										</span>
									</span>
								</div>
								<div class="m-list-settings__item">
									<span class="m-list-settings__item-label">Audit Logs</span>
									<span class="m-list-settings__item-control">
										<span class="m-switch m-switch--outline m-switch--icon-check m-switch--brand">
											<label>
												<input type="checkbox" checked="checked" name="">
												<span></span>
											</label>
										</span>
									</span>
								</div>
							</div>
							<div class="m-list-settings__group">
								<div class="m-list-settings__heading">System Settings</div>
								<div class="m-list-settings__item">
									<span class="m-list-settings__item-label">System Logs</span>
									<span class="m-list-settings__item-control">
										<span class="m-switch m-switch--outline m-switch--icon-check m-switch--brand">
											<label>
												<input type="checkbox" name="">
												<span></span>
											</label>
										</span>
									</span>
								</div>
								<div class="m-list-settings__item">
									<span class="m-list-settings__item-label">Error Reporting</span>
									<span class="m-list-settings__item-control">
										<span class="m-switch m-switch--outline m-switch--icon-check m-switch--brand">
											<label>
												<input type="checkbox" name="">
												<span></span>
											</label>
										</span>
									</span>
								</div>
								<div class="m-list-settings__item">
									<span class="m-list-settings__item-label">Applications Logs</span>
									<span class="m-list-settings__item-control">
										<span class="m-switch m-switch--outline m-switch--icon-check m-switch--brand">
											<label>
												<input type="checkbox" name="">
												<span></span>
											</label>
										</span>
									</span>
								</div>
								<div class="m-list-settings__item">
									<span class="m-list-settings__item-label">Backup Servers</span>
									<span class="m-list-settings__item-control">
										<span class="m-switch m-switch--outline m-switch--icon-check m-switch--brand">
											<label>
												<input type="checkbox" checked="checked" name="">
												<span></span>
											</label>
										</span>
									</span>
								</div>
								<div class="m-list-settings__item">
									<span class="m-list-settings__item-label">Audit Logs</span>
									<span class="m-list-settings__item-control">
										<span class="m-switch m-switch--outline m-switch--icon-check m-switch--brand">
											<label>
												<input type="checkbox" name="">
												<span></span>
											</label>
										</span>
									</span>
								</div>
							</div>
						</div>
					</div>
					<div class="tab-pane" id="m_quick_sidebar_tabs_logs" role="tabpanel">
						<div class="m-list-timeline m-scrollable">
							<div class="m-list-timeline__group">
								<div class="m-list-timeline__heading">
									System Logs
								</div>
								<div class="m-list-timeline__items">
									<div class="m-list-timeline__item">
										<span class="m-list-timeline__badge m-list-timeline__badge--state-success"></span>
										<a href="" class="m-list-timeline__text">12 new users registered <span class="m-badge m-badge--warning m-badge--wide">important</span></a>
										<span class="m-list-timeline__time">Just now</span>
									</div>
									<div class="m-list-timeline__item">
										<span class="m-list-timeline__badge m-list-timeline__badge--state-info"></span>
										<a href="" class="m-list-timeline__text">System shutdown</a>
										<span class="m-list-timeline__time">11 mins</span>
									</div>
									<div class="m-list-timeline__item">
										<span class="m-list-timeline__badge m-list-timeline__badge--state-danger"></span>
										<a href="" class="m-list-timeline__text">New invoice received</a>
										<span class="m-list-timeline__time">20 mins</span>
									</div>
									<div class="m-list-timeline__item">
										<span class="m-list-timeline__badge m-list-timeline__badge--state-warning"></span>
										<a href="" class="m-list-timeline__text">Database overloaded 89% <span class="m-badge m-badge--success m-badge--wide">resolved</span></a>
										<span class="m-list-timeline__time">1 hr</span>
									</div>
									<div class="m-list-timeline__item">
										<span class="m-list-timeline__badge m-list-timeline__badge--state-success"></span>
										<a href="" class="m-list-timeline__text">System error</a>
										<span class="m-list-timeline__time">2 hrs</span>
									</div>
									<div class="m-list-timeline__item">
										<span class="m-list-timeline__badge m-list-timeline__badge--state-info"></span>
										<a href="" class="m-list-timeline__text">Production server down <span class="m-badge m-badge--danger m-badge--wide">pending</span></a>
										<span class="m-list-timeline__time">3 hrs</span>
									</div>
									<div class="m-list-timeline__item">
										<span class="m-list-timeline__badge m-list-timeline__badge--state-success"></span>
										<a href="" class="m-list-timeline__text">Production server up</a>
										<span class="m-list-timeline__time">5 hrs</span>
									</div>
								</div>
							</div>
							<div class="m-list-timeline__group">
								<div class="m-list-timeline__heading">
									Applications Logs
								</div>
								<div class="m-list-timeline__items">
									<div class="m-list-timeline__item">
										<span class="m-list-timeline__badge m-list-timeline__badge--state-info"></span>
										<a href="" class="m-list-timeline__text">New order received <span class="m-badge m-badge--info m-badge--wide">urgent</span></a>
										<span class="m-list-timeline__time">7 hrs</span>
									</div>
									<div class="m-list-timeline__item">
										<span class="m-list-timeline__badge m-list-timeline__badge--state-success"></span>
										<a href="" class="m-list-timeline__text">12 new users registered</a>
										<span class="m-list-timeline__time">Just now</span>
									</div>
									<div class="m-list-timeline__item">
										<span class="m-list-timeline__badge m-list-timeline__badge--state-info"></span>
										<a href="" class="m-list-timeline__text">System shutdown</a>
										<span class="m-list-timeline__time">11 mins</span>
									</div>
									<div class="m-list-timeline__item">
										<span class="m-list-timeline__badge m-list-timeline__badge--state-danger"></span>
										<a href="" class="m-list-timeline__text">New invoices received</a>
										<span class="m-list-timeline__time">20 mins</span>
									</div>
									<div class="m-list-timeline__item">
										<span class="m-list-timeline__badge m-list-timeline__badge--state-warning"></span>
										<a href="" class="m-list-timeline__text">Database overloaded 89%</a>
										<span class="m-list-timeline__time">1 hr</span>
									</div>
									<div class="m-list-timeline__item">
										<span class="m-list-timeline__badge m-list-timeline__badge--state-success"></span>
										<a href="" class="m-list-timeline__text">System error <span class="m-badge m-badge--info m-badge--wide">pending</span></a>
										<span class="m-list-timeline__time">2 hrs</span>
									</div>
									<div class="m-list-timeline__item">
										<span class="m-list-timeline__badge m-list-timeline__badge--state-info"></span>
										<a href="" class="m-list-timeline__text">Production server down</a>
										<span class="m-list-timeline__time">3 hrs</span>
									</div>
								</div>
							</div>
							<div class="m-list-timeline__group">
								<div class="m-list-timeline__heading">
									Server Logs
								</div>
								<div class="m-list-timeline__items">
									<div class="m-list-timeline__item">
										<span class="m-list-timeline__badge m-list-timeline__badge--state-success"></span>
										<a href="" class="m-list-timeline__text">Production server up</a>
										<span class="m-list-timeline__time">5 hrs</span>
									</div>
									<div class="m-list-timeline__item">
										<span class="m-list-timeline__badge m-list-timeline__badge--state-info"></span>
										<a href="" class="m-list-timeline__text">New order received</a>
										<span class="m-list-timeline__time">7 hrs</span>
									</div>
									<div class="m-list-timeline__item">
										<span class="m-list-timeline__badge m-list-timeline__badge--state-success"></span>
										<a href="" class="m-list-timeline__text">12 new users registered</a>
										<span class="m-list-timeline__time">Just now</span>
									</div>
									<div class="m-list-timeline__item">
										<span class="m-list-timeline__badge m-list-timeline__badge--state-info"></span>
										<a href="" class="m-list-timeline__text">System shutdown</a>
										<span class="m-list-timeline__time">11 mins</span>
									</div>
									<div class="m-list-timeline__item">
										<span class="m-list-timeline__badge m-list-timeline__badge--state-danger"></span>
										<a href="" class="m-list-timeline__text">New invoice received</a>
										<span class="m-list-timeline__time">20 mins</span>
									</div>
									<div class="m-list-timeline__item">
										<span class="m-list-timeline__badge m-list-timeline__badge--state-warning"></span>
										<a href="" class="m-list-timeline__text">Database overloaded 89%</a>
										<span class="m-list-timeline__time">1 hr</span>
									</div>
									<div class="m-list-timeline__item">
										<span class="m-list-timeline__badge m-list-timeline__badge--state-success"></span>
										<a href="" class="m-list-timeline__text">System error</a>
										<span class="m-list-timeline__time">2 hrs</span>
									</div>
									<div class="m-list-timeline__item">
										<span class="m-list-timeline__badge m-list-timeline__badge--state-info"></span>
										<a href="" class="m-list-timeline__text">Production server down</a>
										<span class="m-list-timeline__time">3 hrs</span>
									</div>
									<div class="m-list-timeline__item">
										<span class="m-list-timeline__badge m-list-timeline__badge--state-success"></span>
										<a href="" class="m-list-timeline__text">Production server up</a>
										<span class="m-list-timeline__time">5 hrs</span>
									</div>
									<div class="m-list-timeline__item">
										<span class="m-list-timeline__badge m-list-timeline__badge--state-info"></span>
										<a href="" class="m-list-timeline__text">New order received</a>
										<span class="m-list-timeline__time">1117 hrs</span>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<!-- end::Quick Sidebar -->

		<!-- begin::Scroll Top -->
		<div id="m_scroll_top" class="m-scroll-top">
			<i class="la la-arrow-up"></i>
		</div>

		<!-- end::Scroll Top -->

		<!-- begin::Quick Nav -->
		<!--<ul class="m-nav-sticky" style="margin-top: 30px;">
			<li class="m-nav-sticky__item" data-toggle="m-tooltip" title="Purchase" data-placement="left">
				<a href="https://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes" target="_blank"><i class="la la-cart-arrow-down"></i></a>
			</li>
			<li class="m-nav-sticky__item" data-toggle="m-tooltip" title="Documentation" data-placement="left">
				<a href="https://keenthemes.com/metronic/documentation.html" target="_blank"><i class="la la-code-fork"></i></a>
			</li>
			<li class="m-nav-sticky__item" data-toggle="m-tooltip" title="Support" data-placement="left">
				<a href="https://keenthemes.com/forums/forum/support/metronic5/" target="_blank"><i class="la la-life-ring"></i></a>
			</li>
		</ul>-->

		<!-- begin::Quick Nav -->

		<!--begin::Global Theme Bundle -->
        <script>
        $(document).ready(function(){
    var next = 1;
    $(".add-more").click(function(e){
        e.preventDefault();
        var addto = "#field" + next;
        var addRemove = "#field" + (next);
        next = next + 1;
        var newIn = '<input autocomplete="off" class="input form-control" id="field' + next + '" name="field' + next + '" type="text">';
        var newInput = $(newIn);
        var removeBtn = '<button id="remove' + (next - 1) + '" class="btn btn-danger remove-me" >-</button></div><div id="field">';
        var removeButton = $(removeBtn);
        $(addto).after(newInput);
        $(addRemove).after(removeButton);
        $("#field" + next).attr('data-source',$(addto).attr('data-source'));
        $("#count").val(next);  
        
            $('.remove-me').click(function(e){
                e.preventDefault();
                var fieldNum = this.id.charAt(this.id.length-1);
                var fieldID = "#field" + fieldNum;
                $(this).remove();
                $(fieldID).remove();
            });
    });
    

    
});

        </script>
        
        <script>
			
            $(document).ready(function() {
                $('#txtEditor').summernote({
                    placeholder: 'DMS',
                    tabsize: 2,
                    height: 400
                });
            
                $('#txtEditor2').summernote({
                    placeholder: 'DMS',
                    tabsize: 2,
                    height: 400
                });
            
                $('#txtEditor3').summernote({
                    placeholder: 'DMS',
                    tabsize: 2,
                    height: 400
                });
            
                $('#txtEditor4').summernote({
                    placeholder: 'DMS',
                    tabsize: 2,
                    height: 400
                });
            });
            
            
		</script>
		<script src="assets/admin/assets/vendors/base/vendors.bundle.js" type="text/javascript"></script>
		<script src="assets/admin/assets/demo/demo3/base/scripts.bundle.js" type="text/javascript"></script>
        <!-- include summernote css/js -->

<script src="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote.js"></script>

		<!--end::Global Theme Bundle -->

		<!--begin::Page Scripts -->
		<script src="assets/admin/assets/app/js/dashboard.js" type="text/javascript"></script>
        <!-- jQuery library -->
      <!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>-->

<!-- Latest compiled JavaScript -->
<!--<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>-->

		<!--end::Page Scripts -->
	</body>

	<!-- end::Body -->
</html>