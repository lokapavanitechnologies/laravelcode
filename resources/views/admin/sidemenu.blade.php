<div id="m_ver_menu" class="m-aside-menu  m-aside-menu--skin-dark m-aside-menu--submenu-skin-dark m-aside-menu--dropdown " data-menu-vertical="true" m-menu-dropdown="1" m-menu-scrollable="0" m-menu-dropdown-timeout="500">
                  <ul class="m-menu__nav  m-menu__nav--dropdown-submenu-arrow ">
                     <li class="m-menu__item  m-menu__item--active" aria-haspopup="true">
                        <a href="index.php" class="m-menu__link ">
                        <span class="m-menu__item-here"></span>
                        <i class="m-menu__link-icon flaticon-squares"></i>
                        <span class="m-menu__link-text">Dashboard</span>
                        </a>
                     </li>
                    
                      
                      <li class="m-menu__item m-menu__item--submenu m-menu__item--open" aria-haspopup="true" m-menu-submenu-toggle="hover">
                     <a href="users.php" class="m-menu__link m-menu__toggle">
                     <i class="m-menu__link-icon flaticon-users"></i><span class="m-menu__link-text">Organization</span>
                    <i class="m-menu__ver-arrow la la-angle-right"></i></a>
                      <div class="m-menu__submenu " m-hidden-height="280" style="">
                <span class="m-menu__arrow"></span>
             <ul class="m-menu__subnav">
         <li class="m-menu__item  m-menu__item--parent" aria-haspopup="true"><span class="m-menu__link"><span class="m-menu__link-text">List of   Users</span></span></li>
         <li class="m-menu__item " aria-haspopup="true"><a href="userslist.php" class="m-menu__link "><i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i><span class="m-menu__link-text">List of Users</span></a></li>
         <li class="m-menu__item " aria-haspopup="true"><a href="users.php" class="m-menu__link "><i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i><span class="m-menu__link-text">Add New User</span></a></li>
         
                  </ul>
               </div>
            </li>
                      <li class="m-menu__item  m-menu__item--" aria-haspopup="true">
                        <a href="policy.php" class="m-menu__link ">
                        <span class="m-menu__item-here"></span>
                        <i class="m-menu__link-icon flaticon-security"></i>
                        <span class="m-menu__link-text">Policy</span>
                        </a>
                     </li>
                      <!--<li class="m-menu__item  m-menu__item--" aria-haspopup="true">
                        <a href="alldocuments.php" class="m-menu__link ">
                        <span class="m-menu__item-here"></span>
                        <i class="m-menu__link-icon flaticon-multimedia-2"></i>
                        <span class="m-menu__link-text">Documents</span>
                        </a>
                     </li>-->
                      
                     
                      
                      <li class="m-menu__item m-menu__item--submenu m-menu__item--open" aria-haspopup="true" m-menu-submenu-toggle="hover">
                     <a href="alldocuments.php" class="m-menu__link m-menu__toggle">
                     <i class="m-menu__link-icon flaticon-multimedia-2"></i><span class="m-menu__link-text">Documents</span>
                    <i class="m-menu__ver-arrow la la-angle-right"></i></a>
                      <div class="m-menu__submenu " m-hidden-height="280" style="">
                <span class="m-menu__arrow"></span>
             <ul class="m-menu__subnav">
         <li class="m-menu__item  m-menu__item--parent" aria-haspopup="true"><span class="m-menu__link"><span class="m-menu__link-text">List of   Users</span></span></li>
         <li class="m-menu__item " aria-haspopup="true"><a href="alldocuments.php" class="m-menu__link "><i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i><span class="m-menu__link-text">All Documents</span></a></li>
         <li class="m-menu__item " aria-haspopup="true"><a href="upload.php" class="m-menu__link "><i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i><span class="m-menu__link-text">Add New Document</span></a></li>
         
                  </ul>
               </div>
            </li>
                      
                      
                      
                      
                  </ul>
               </div>