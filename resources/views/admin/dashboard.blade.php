<!DOCTYPE html>
<!-- 
   Template Name: Metronic - Responsive Admin Dashboard Template build with Twitter Bootstrap 4
   Author: KeenThemes
   Website: http://www.keenthemes.com/
   Contact: support@keenthemes.com
   Follow: www.twitter.com/keenthemes
   Dribbble: www.dribbble.com/keenthemes
   Like: www.facebook.com/keenthemes
   Purchase: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
   Renew Support: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
   License: You must have a valid license purchased only from themeforest(the above link) in order to legally use the theme for your project.
   -->
@include('layout.headerscripts')
   <!-- end::Head -->
   <!-- begin::Body -->
   <body class="m-page--fluid m--skin- m-content--skin-light2 m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default">
      <!-- begin:: Page -->
      <div class="m-grid m-grid--hor m-grid--root m-page">
         <!-- BEGIN: Header -->
      	@include('layout.header')

         <!-- END: Header -->
         <!-- begin::Body -->
         <div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">
            <!-- BEGIN: Left Aside -->
            <button class="m-aside-left-close m-aside-left-close--skin-dark" id="m_aside_left_close_btn">
            <i class="la la-close"></i>
            </button>
            <div id="m_aside_left" class="m-grid__item	m-aside-left  m-aside-left--skin-dark ">
               <!-- BEGIN: Aside Menu -->
               @include('layout.sidemenu')
               <!-- END: Aside Menu -->
            </div>
            <!-- END: Left Aside -->
            <div class="m-grid__item m-grid__item--fluid m-wrapper">
               <!-- BEGIN: Subheader -->
               <div class="m-subheader ">
                  <div class="d-flex align-items-center">
                     <div class="mr-auto">
                        <h3 class="m-subheader__title ">Dashboard</h3>
                     </div>
                     <div>
                        <span class="m-subheader__daterange" id="m_dashboard_daterangepicker">
                        <span class="m-subheader__daterange-label">
                        <span class="m-subheader__daterange-title"></span>
                        <span class="m-subheader__daterange-date m--font-brand"></span>
                        </span>
                        <a href="#" class="btn btn-sm btn-brand m-btn m-btn--icon m-btn--icon-only m-btn--custom m-btn--pill">
                        <i class="la la-angle-down"></i>
                        </a>
                        </span>
                     </div>
                  </div>
               </div>
               <!-- END: Subheader -->
               <div class="m-content">
                  <!--Begin::Section-->
                  <div class="row">
                     <div class="col-xl-4">
                        <!--begin:: Widgets/Quick Stats-->
                         <div class="m-portlet m--bg-danger m-portlet--bordered-semi m-portlet--skin-dark m-portlet--full-height ">
                           <div class="m-portlet__head">
                              <div class="m-portlet__head-caption">
                                 <div class="m-portlet__head-title">
                                    <h3 class="m-portlet__head-text">
                                       Announcements
                                    </h3>
                                 </div>
                              </div>
                              <div class="m-portlet__head-tools">
                                 <ul class="m-portlet__nav">
                                    <li class="m-portlet__nav-item m-dropdown m-dropdown--inline m-dropdown--arrow m-dropdown--align-right m-dropdown--align-push" m-dropdown-toggle="hover">
                                       <a href="#" class="m-portlet__nav-link m-portlet__nav-link--icon m-portlet__nav-link--icon-xl">
                                       <i class="la la-ellipsis-h m--font-light"></i>
                                       </a>
                                       <div class="m-dropdown__wrapper">
                                          <span class="m-dropdown__arrow m-dropdown__arrow--right m-dropdown__arrow--adjust"></span>
                                          <div class="m-dropdown__inner">
                                             <div class="m-dropdown__body">
                                                <div class="m-dropdown__content">
                                                   <ul class="m-nav">
                                                      <li class="m-nav__section m-nav__section--first">
                                                         <span class="m-nav__section-text">Quick Actions</span>
                                                      </li>
                                                      <li class="m-nav__item">
                                                         <a href="" class="m-nav__link">
                                                         <i class="m-nav__link-icon flaticon-share"></i>
                                                         <span class="m-nav__link-text">Activity</span>
                                                         </a>
                                                      </li>
                                                      <li class="m-nav__item">
                                                         <a href="" class="m-nav__link">
                                                         <i class="m-nav__link-icon flaticon-chat-1"></i>
                                                         <span class="m-nav__link-text">Messages</span>
                                                         </a>
                                                      </li>
                                                      <li class="m-nav__item">
                                                         <a href="" class="m-nav__link">
                                                         <i class="m-nav__link-icon flaticon-info"></i>
                                                         <span class="m-nav__link-text">FAQ</span>
                                                         </a>
                                                      </li>
                                                      <li class="m-nav__item">
                                                         <a href="" class="m-nav__link">
                                                         <i class="m-nav__link-icon flaticon-lifebuoy"></i>
                                                         <span class="m-nav__link-text">Support</span>
                                                         </a>
                                                      </li>
                                                      <li class="m-nav__separator m-nav__separator--fit"></li>
                                                      <li class="m-nav__item">
                                                         <a href="#" class="btn btn-outline-danger m-btn m-btn--pill m-btn--wide btn-sm">Cancel</a>
                                                      </li>
                                                   </ul>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </li>
                                 </ul>
                              </div>
                           </div>
                           <div class="m-portlet__body">
                              <!--begin::Widget 7-->
                              <div class="m-widget7 m-widget7--skin-dark">
                                 <div class="m-widget7__desc">
                                    Lorem ipsum dolor sit amet,consectetuer edipiscing elit,sed diam nonummy euismod tinciduntut laoreet doloremagna
                                 </div>
                                 <div class="m-widget7__user">
                                    <div class="m-widget7__user-img">
                                       <img class="m-widget7__img" src="assets/admin/assets/app/media/img//users/100_5.jpg" alt="">
                                    </div>
                                    <div class="m-widget7__info">
                                       <span class="m-widget7__username">
                                       Nick Mana
                                       </span>
                                       <br>
                                       <span class="m-widget7__time">
                                       6 hours ago
                                       </span>
                                    </div>
                                 </div>
                                 <div class="m-widget7__button">
                                    <a class="m-btn m-btn--pill btn btn-accent" href="#" role="button">All Feeds</a>
                                 </div>
                              </div>
                              <!--end::Widget 7-->
                           </div>
                        </div>
                         
                         
                        <!--<div class="row m-row--full-height">
                           <div class="col-sm-12 col-md-12 col-lg-6">
                              <div class="m-portlet m-portlet--half-height m-portlet--border-bottom-brand ">
                                 <div class="m-portlet__body">
                                    <div class="m-widget26">
                                       <div class="m-widget26__number">
                                          570
                                          <small>Admin Uploads</small>
                                       </div>
                                       <div class="m-widget26__chart" style="height:90px; width: 220px;">
                                          <canvas id="m_chart_quick_stats_1"></canvas>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="m--space-30"></div>
                              <div class="m-portlet m-portlet--half-height m-portlet--border-bottom-danger ">
                                 <div class="m-portlet__body">
                                    <div class="m-widget26">
                                       <div class="m-widget26__number">
                                          30
                                          <small>Staff Uploads</small>
                                       </div>
                                       <div class="m-widget26__chart" style="height:90px; width: 220px;">
                                          <canvas id="m_chart_quick_stats_2"></canvas>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="col-sm-12 col-md-12 col-lg-6">
                              <div class="m-portlet m-portlet--half-height m-portlet--border-bottom-success ">
                                 <div class="m-portlet__body">
                                    <div class="m-widget26">
                                       <div class="m-widget26__number">
                                          230
                                          <small>Practice Uploads</small>
                                       </div>
                                       <div class="m-widget26__chart" style="height:90px; width: 220px;">
                                          <canvas id="m_chart_quick_stats_3"></canvas>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="m--space-30"></div>
                              <div class="m-portlet m-portlet--half-height m-portlet--border-bottom-accent ">
                                 <div class="m-portlet__body">
                                    <div class="m-widget26">
                                       <div class="m-widget26__number">
                                          50
                                          <small>Others Uploads</small>
                                       </div>
                                       <div class="m-widget26__chart" style="height:90px; width: 220px;">
                                          <canvas id="m_chart_quick_stats_4"></canvas>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>-->
                        <!--end:: Widgets/Quick Stats-->
                     </div>
                     <div class="col-xl-4">
                        <!--begin:: Widgets/Finance Summary-->
                         
                         <div class="m-portlet m-portlet--full-height ">
	<div class="m-portlet__head">
		<div class="m-portlet__head-caption">
			<div class="m-portlet__head-title">
				<h3 class="m-portlet__head-text">
					Recent Uploads
				</h3>
			</div>
		</div>
		<div class="m-portlet__head-tools">
			<ul class="m-portlet__nav">
				<li class="m-portlet__nav-item m-dropdown m-dropdown--inline m-dropdown--arrow m-dropdown--align-right m-dropdown--align-push" m-dropdown-toggle="hover">
					<a href="#" class="m-portlet__nav-link m-dropdown__toggle dropdown-toggle btn btn--sm m-btn--pill btn-secondary m-btn m-btn--label-brand">
					Today
					</a>
					<div class="m-dropdown__wrapper">
						<span class="m-dropdown__arrow m-dropdown__arrow--right m-dropdown__arrow--adjust"></span>
						<div class="m-dropdown__inner">
							<div class="m-dropdown__body">
								<div class="m-dropdown__content">
									<ul class="m-nav">
										<li class="m-nav__section m-nav__section--first">
											<span class="m-nav__section-text">Quick Actions</span>
										</li>
										<li class="m-nav__item">
											<a href="" class="m-nav__link">
											<i class="m-nav__link-icon flaticon-share"></i>
											<span class="m-nav__link-text">Activity</span>
											</a>
										</li>
										<li class="m-nav__item">
											<a href="" class="m-nav__link">
											<i class="m-nav__link-icon flaticon-chat-1"></i>
											<span class="m-nav__link-text">Messages</span>
											</a>
										</li>
										<li class="m-nav__item">
											<a href="" class="m-nav__link">
											<i class="m-nav__link-icon flaticon-info"></i>
											<span class="m-nav__link-text">FAQ</span>
											</a>
										</li>
										<li class="m-nav__item">
											<a href="" class="m-nav__link">
											<i class="m-nav__link-icon flaticon-lifebuoy"></i>
											<span class="m-nav__link-text">Support</span>
											</a>
										</li>
										<li class="m-nav__separator m-nav__separator--fit">
										</li>
										<li class="m-nav__item">
											<a href="#" class="btn btn-outline-danger m-btn m-btn--pill m-btn--wide btn-sm">Cancel</a>
										</li>
									</ul>
								</div>
							</div>
						</div>
					</div>
				</li>
			</ul>
		</div>
	</div>
	<div class="m-portlet__body">
		<!--begin::m-widget4-->
		<div class="m-widget4">
			<div class="m-widget4__item">
				<div class="m-widget4__img m-widget4__img--icon">							 
					<img src="assets/admin/assets/app/media/img/files/doc.svg" alt="">  
				</div>
				<div class="m-widget4__info">
					<span class="m-widget4__text">
					bais document 1
					</span> 							 		 
				</div>
				<div class="m-widget4__ext">
					<a href="#" class="m-widget4__icon">
						<i class="la la-download"></i>
					</a>
				</div>
			</div>
			<div class="m-widget4__item">
				<div class="m-widget4__img m-widget4__img--icon">							 
					<img src="assets/admin/assets/app/media/img/files/jpg.svg" alt=""> 
				</div>
				<div class="m-widget4__info">
					<span class="m-widget4__text">
					bais document 1
					</span> 								 
				</div>
				<div class="m-widget4__ext">
					<a href="#" class="m-widget4__icon">
						<i class="la la-download"></i>
					</a>
				</div>
			</div>
			<div class="m-widget4__item">
				<div class="m-widget4__img m-widget4__img--icon">							 
					<img src="assets/admin/assets/app/media/img/files/pdf.svg" alt="">   
				</div>
				<div class="m-widget4__info">
					<span class="m-widget4__text">
					bais document 1
					</span> 							 	 
				</div>
				<div class="m-widget4__ext">
					<a href="#" class="m-widget4__icon">
						<i class="la la-download"></i>
					</a>
				</div>
			</div>
			<div class="m-widget4__item">
				<div class="m-widget4__img m-widget4__img--icon">							 
					<img src="assets/admin/assets/app/media/img/files/javascript.svg" alt=""> 
				</div>
				<div class="m-widget4__info">
					<span class="m-widget4__text">
					bais document 1
					</span>									 
				</div>
				<div class="m-widget4__ext">
					<a href="#" class="m-widget4__icon">
						<i class="la la-download"></i>
					</a>
				</div>
			</div>
			<div class="m-widget4__item">
				<div class="m-widget4__img m-widget4__img--icon">							 
					<img src="assets/admin/assets/app/media/img/files/zip.svg" alt=""> 
				</div>
				<div class="m-widget4__info">
					<span class="m-widget4__text">
					bais document 1
					</span>								 
				</div>
				<div class="m-widget4__ext">
					<a href="#" class="m-widget4__icon">
						<i class="la la-download"></i>
					</a>
				</div>
			</div>
			<div class="m-widget4__item">
				<div class="m-widget4__img m-widget4__img--icon">							 
					<img src="assets/admin/assets/app/media/img/files/pdf.svg" alt=""> 
				</div>
				<div class="m-widget4__info">
					<span class="m-widget4__text">
					bais document 1
					</span>								 
				</div>
				<div class="m-widget4__ext">
					<a href="#" class="m-widget4__icon">
						<i class="la la-download"></i>
					</a>
				</div>
			</div>
		</div>
		<!--end::Widget 9-->
	</div>
</div>
                         
                         
                        <!--<div class="m-portlet m-portlet--full-height m-portlet--fit ">
                           <div class="m-portlet__head">
                              <div class="m-portlet__head-caption">
                                 <div class="m-portlet__head-title">
                                    <h3 class="m-portlet__head-text">
                                       Documents uploads Summary
                                    </h3>
                                 </div>
                              </div>
                              <div class="m-portlet__head-tools">
                                 <ul class="nav nav-pills nav-pills--brand m-nav-pills--align-right m-nav-pills--btn-pill m-nav-pills--btn-sm" role="tablist">
                                    <li class="nav-item m-tabs__item">
                                       <a class="nav-link m-tabs__link active" data-toggle="tab" href="#m_widget4_tab1_content" role="tab">
                                       Month
                                       </a>
                                    </li>
                                    <li class="nav-item m-tabs__item">
                                       <a class="nav-link m-tabs__link" data-toggle="tab" href="#m_widget4_tab2_content" role="tab">
                                       All Time
                                       </a>
                                    </li>
                                 </ul>
                              </div>
                           </div>
                           <div class="m-portlet__body">
                              <div class="tab-content">
                                 <div class="tab-pane active">
                                    <div class="m-widget12 m-widget12--chart-bottom m--margin-top-10" style="min-height: 450px">
                                       <div class="m-widget12__item">
                                          <span class="m-widget12__text1">Present Documents 
                                          <br>
                                          <span>500</span>
                                          </span>
                                          <span class="m-widget12__text2">Last Month
                                          <br>
                                          <span>July 24,2017</span>
                                          </span>
                                       </div>
                                       <div class="m-widget12__item">
                                          <span class="m-widget12__text1">Documents
                                          <br>
                                          <span>70</span>
                                          </span>
                                          <div class="m-widget12__text2">
                                             <div class="m-widget12__desc">View Rate</div>
                                             <br>
                                             <div class="m-widget12__progress">
                                                <div class="m-widget12__progress-sm progress m-progress--sm">
                                                   <div class="m-widget12__progress-bar progress-bar bg-brand" role="progressbar" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                                </div>
                                                <span class="m-widget12__stats">
                                                63%
                                                </span>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="m-widget12__chart m-portlet-fit--sides" style="height:290px;">
                                          <canvas id="m_chart_finance_summary"></canvas>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="tab-pane"></div>
                              </div>
                           </div>
                        </div>-->
                        <!--end:: Widgets/Finance Summary-->
                     </div>
                     <div class="col-xl-4">
                        <!--begin:: Widgets/Top Products-->
                        <div class="m-portlet m-portlet--full-height m-portlet--fit ">
                           <div class="m-portlet__head">
                              <div class="m-portlet__head-caption">
                                 <div class="m-portlet__head-title">
                                    <h3 class="m-portlet__head-text">
                                      Registered Persons
                                    </h3>
                                 </div>
                              </div>
                              <div class="m-portlet__head-tools">
                                 <ul class="m-portlet__nav">
                                    <li class="m-portlet__nav-item m-dropdown m-dropdown--inline m-dropdown--arrow m-dropdown--align-right m-dropdown--align-push" m-dropdown-toggle="hover" aria-expanded="true">
                                       <a href="#" class="m-portlet__nav-link m-dropdown__toggle dropdown-toggle btn btn--sm m-btn--pill btn-secondary m-btn m-btn--label-brand">
                                       All
                                       </a>
                                       <div class="m-dropdown__wrapper">
                                          <span class="m-dropdown__arrow m-dropdown__arrow--right m-dropdown__arrow--adjust" style="left: auto; right: 36.5px;"></span>
                                          <div class="m-dropdown__inner">
                                             <div class="m-dropdown__body">
                                                <div class="m-dropdown__content">
                                                   <ul class="m-nav">
                                                      <li class="m-nav__item">
                                                         <a href="" class="m-nav__link">
                                                         <i class="m-nav__link-icon flaticon-share"></i>
                                                         <span class="m-nav__link-text">Activity</span>
                                                         </a>
                                                      </li>
                                                      <li class="m-nav__item">
                                                         <a href="" class="m-nav__link">
                                                         <i class="m-nav__link-icon flaticon-chat-1"></i>
                                                         <span class="m-nav__link-text">Messages</span>
                                                         </a>
                                                      </li>
                                                      <li class="m-nav__item">
                                                         <a href="" class="m-nav__link">
                                                         <i class="m-nav__link-icon flaticon-info"></i>
                                                         <span class="m-nav__link-text">FAQ</span>
                                                         </a>
                                                      </li>
                                                      <li class="m-nav__item">
                                                         <a href="" class="m-nav__link">
                                                         <i class="m-nav__link-icon flaticon-lifebuoy"></i>
                                                         <span class="m-nav__link-text">Support</span>
                                                         </a>
                                                      </li>
                                                   </ul>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </li>
                                 </ul>
                              </div>
                           </div>
                           <div class="m-portlet__body">
                              <!--begin::Widget5-->
                              <div class="m-widget4 m-widget4--chart-bottom" style="min-height: 480px">
                                 <div class="m-widget4__item">
                                    <div class="m-widget4__img m-widget4__img--logo">
                                       <img class="m-widget7__img" src="assets/admin/assets/app/media/img//users/100_5.jpg" alt="">
                                    </div>
                                    <div class="m-widget4__info">
                                       <span class="m-widget4__title">
                                       Registered Doctors 
                                       </span>
                                       <br>
                                       <span class="m-widget4__sub">
                                       Duty and Surgeons
                                       </span>
                                    </div>
                                    <span class="m-widget4__ext">
                                    <span class="m-widget4__number m--font-brand">17</span>
                                    </span>
                                 </div>
                                 <div class="m-widget4__item">
                                    <div class="m-widget4__img m-widget4__img--logo">
                                      <img class="m-widget7__img" src="assets/admin/assets/app/media/img//users/100_5.jpg" alt="">
                                    </div>
                                    <div class="m-widget4__info">
                                       <span class="m-widget4__title">
                                      Registered Parctices 
                                       </span>
                                       <br>
                                       <span class="m-widget4__sub">
                                      Practice persons
                                       </span>
                                    </div>
                                    <span class="m-widget4__ext">
                                    <span class="m-widget4__number m--font-brand">300</span>
                                    </span>
                                 </div>
                                 <div class="m-widget4__item">
                                    <div class="m-widget4__img m-widget4__img--logo">
                                      <img class="m-widget7__img" src="assets/admin/assets/app/media/img//users/100_5.jpg" alt="">
                                    </div>
                                    <div class="m-widget4__info">
                                       <span class="m-widget4__title">
                                       All Staff
                                       </span>
                                       <br>
                                       <span class="m-widget4__sub">
                                       Doctors and Staff
                                       </span>
                                    </div>
                                    <span class="m-widget4__ext">
                                    <span class="m-widget4__number m--font-brand">90</span>
                                    </span>
                                 </div>
                                 <div class="m-widget4__chart m-portlet-fit--sides m--margin-top-20" style="height:260px;">
                                    <canvas id="m_chart_trends_stats_2"></canvas>
                                 </div>
                              </div>
                              <!--end::Widget 5-->
                           </div>
                        </div>
                        <!--end:: Widgets/Top Products-->
                     </div>
                  </div>
                  <!--End::Section-->
                  <!--Begin::Section-->
                  <div class="row" style="padding-bottom:30px;">
                     <div class="col-xl-12">
              <div class="card">
          <div class="header">
            <h2>
              Documents pending Approval             
            </h2>
            <!--<ul class="header-dropdown m-r--5">
                                  <button type="button" class="btn btn-lg btn-primary waves-effect modalButtonUser" data-toggle="modal"><i class="material-icons">add</i> Add User</button>
                                  <button type="button" class="btn btn-lg btn-primary waves-effect InviteUser" data-toggle="modal"><i class="material-icons">add</i>Invite People</button>
                            </ul>-->
          </div>
          <!-- /.box-header -->
          <div class="m-portlet m-portlet--mobile">
	
	<div class="m-portlet__body">
		<!--begin: Search Form -->

		
		<!--begin: Datatable -->
		<div id="m_table_1_wrapper" class="dataTables_wrapper dt-bootstrap4"><div class="row"><div class="col-sm-12"><table class="table table-striped- table-bordered table-hover table-checkable dataTable dtr-inline" id="m_table_1" role="grid" aria-describedby="m_table_1_info" style="width: 974px;">
								<thead>
			  						<tr role="row"><th class="sorting_asc" tabindex="0" aria-controls="m_table_1" rowspan="1" colspan="1" style="width: 65.25px;" aria-sort="ascending" aria-label="Record ID: activate to sort column descending">Document ID</th><th class="sorting" tabindex="0" aria-controls="m_table_1" rowspan="1" colspan="1" style="width: 59.25px;" aria-label="Order ID: activate to sort column ascending">Document Name</th><th class="sorting" tabindex="0" aria-controls="m_table_1" rowspan="1" colspan="1" style="width: 56.25px;" aria-label="Country: activate to sort column ascending">Permission</th><th class="sorting" tabindex="0" aria-controls="m_table_1" rowspan="1" colspan="1" style="width: 101.25px;" aria-label="Ship City: activate to sort column ascending">Category</th><th class="sorting" tabindex="0" aria-controls="m_table_1" rowspan="1" colspan="1" style="width: 113.25px;" aria-label="Company Agent: activate to sort column ascending">Designation</th><th class="sorting" tabindex="0" aria-controls="m_table_1" rowspan="1" colspan="1" style="width: 67.25px;" aria-label="Ship Date: activate to sort column ascending">Create Date</th><th class="sorting" tabindex="0" aria-controls="m_table_1" rowspan="1" colspan="1" style="width: 57.25px;" aria-label="Status: activate to sort column ascending">Status</th><th class="sorting" tabindex="0" aria-controls="m_table_1" rowspan="1" colspan="1" style="width: 67.25px;" aria-label="Ship Date: activate to sort column ascending">Expiry Date</th><th class="sorting_disabled" rowspan="1" colspan="1" style="width: 73.5px;" aria-label="Actions">Actions</th></tr>
						</thead>
			
			
								<tfoot>
					<tr><th rowspan="1" colspan="1">Document ID</th><th rowspan="1" colspan="1">Document Name</th><th rowspan="1" colspan="1">Permission</th><th rowspan="1" colspan="1">Category</th><th rowspan="1" colspan="1">Designation</th><th rowspan="1" colspan="1">Create Date</th><th rowspan="1" colspan="1">Status</th><th rowspan="1" colspan="1">Expiry Date</th><th rowspan="1" colspan="1">Actions</th></tr>
					</tfoot>
					<tbody><tr role="row" class="odd"><td class="sorting_1" tabindex="0">1</td><td>Name1</td><td>read</td><td>Staff</td><td>Doctor</td><td>2/12/2018</td><td><span class="m-badge  m-badge--primary m-badge--wide">Apporved</span></td><td><span class="m-badge m-badge--danger m-badge--dot"></span>&nbsp;<span class="m--font-bold m--font-danger">1/12/2019</span></td>
                        <td>
                        <span class="dropdown">
                            <a href="#" class="btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill" data-toggle="dropdown" aria-expanded="true">
                              <i class="la la-ellipsis-h"></i>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right">
                                <a class="dropdown-item" href="#"><i class="la la-edit"></i> Edit Details</a>
                                <a class="dropdown-item" href="#"><i class="la la-leaf"></i> Apporval</a>
                                <a class="dropdown-item" href="#"><i class="la la-print"></i> Rejected</a>
                            </div>
                        </span>
                        <a href="#" class="m-portlet__nav-link btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill" title="View">
                          <i class="la la-edit"></i>
                        </a></td></tr><tr role="row" class="even"><td class="sorting_1" tabindex="0">1</td><td>Name1</td><td>read</td><td>Staff</td><td>Doctor</td><td>2/12/2018</td><td><span class="m-badge  m-badge--primary m-badge--wide">Apporved</span></td><td><span class="m-badge m-badge--danger m-badge--dot"></span>&nbsp;<span class="m--font-bold m--font-danger">1/12/2019</span></td>
                        <td>
                        <span class="dropdown">
                            <a href="#" class="btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill" data-toggle="dropdown" aria-expanded="true">
                              <i class="la la-ellipsis-h"></i>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right">
                                <a class="dropdown-item" href="#"><i class="la la-edit"></i> Edit Details</a>
                                <a class="dropdown-item" href="#"><i class="la la-leaf"></i> Apporval</a>
                                <a class="dropdown-item" href="#"><i class="la la-print"></i> Rejected</a>
                            </div>
                        </span>
                        <a href="#" class="m-portlet__nav-link btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill" title="View">
                          <i class="la la-edit"></i>
                        </a></td></tr><tr role="row" class="odd"><td class="sorting_1" tabindex="0">1</td><td>Name1</td><td>read</td><td>Staff</td><td>Doctor</td><td>2/12/2018</td><td><span class="m-badge  m-badge--primary m-badge--wide">Apporved</span></td><td><span class="m-badge m-badge--danger m-badge--dot"></span>&nbsp;<span class="m--font-bold m--font-danger">1/12/2019</span></td>
                        <td>
                        <span class="dropdown">
                            <a href="#" class="btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill" data-toggle="dropdown" aria-expanded="true">
                              <i class="la la-ellipsis-h"></i>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right">
                                <a class="dropdown-item" href="#"><i class="la la-edit"></i> Edit Details</a>
                                <a class="dropdown-item" href="#"><i class="la la-leaf"></i> Apporval</a>
                                <a class="dropdown-item" href="#"><i class="la la-print"></i> Rejected</a>
                            </div>
                        </span>
                        <a href="#" class="m-portlet__nav-link btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill" title="View">
                          <i class="la la-edit"></i>
                        </a></td></tr><tr role="row" class="even"><td class="sorting_1" tabindex="0">1</td><td>Name1</td><td>read</td><td>Staff</td><td>Doctor</td><td>2/12/2018</td><td><span class="m-badge  m-badge--primary m-badge--wide">Apporved</span></td><td><span class="m-badge m-badge--danger m-badge--dot"></span>&nbsp;<span class="m--font-bold m--font-danger">1/12/2019</span></td>
                        <td>
                        <span class="dropdown">
                            <a href="#" class="btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill" data-toggle="dropdown" aria-expanded="true">
                              <i class="la la-ellipsis-h"></i>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right">
                                <a class="dropdown-item" href="#"><i class="la la-edit"></i> Edit Details</a>
                                <a class="dropdown-item" href="#"><i class="la la-leaf"></i> Apporval</a>
                                <a class="dropdown-item" href="#"><i class="la la-print"></i> Rejected</a>
                            </div>
                        </span>
                        <a href="#" class="m-portlet__nav-link btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill" title="View">
                          <i class="la la-edit"></i>
                        </a></td></tr><tr role="row" class="odd"><td class="sorting_1" tabindex="0">1</td><td>Name1</td><td>read</td><td>Staff</td><td>Doctor</td><td>2/12/2018</td><td><span class="m-badge  m-badge--primary m-badge--wide">Apporved</span></td><td><span class="m-badge m-badge--danger m-badge--dot"></span>&nbsp;<span class="m--font-bold m--font-danger">1/12/2019</span></td>
                        <td>
                        <span class="dropdown">
                            <a href="#" class="btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill" data-toggle="dropdown" aria-expanded="true">
                              <i class="la la-ellipsis-h"></i>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right">
                                <a class="dropdown-item" href="#"><i class="la la-edit"></i> Edit Details</a>
                                <a class="dropdown-item" href="#"><i class="la la-leaf"></i> Apporval</a>
                                <a class="dropdown-item" href="#"><i class="la la-print"></i> Rejected</a>
                            </div>
                        </span>
                        <a href="#" class="m-portlet__nav-link btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill" title="View">
                          <i class="la la-edit"></i>
                        </a></td></tr><tr role="row" class="even"><td class="sorting_1" tabindex="0">1</td><td>Name1</td><td>read</td><td>Staff</td><td>Doctor</td><td>2/12/2018</td><td><span class="m-badge  m-badge--primary m-badge--wide">Apporved</span></td><td><span class="m-badge m-badge--danger m-badge--dot"></span>&nbsp;<span class="m--font-bold m--font-danger">1/12/2019</span></td>
                        <td>
                        <span class="dropdown">
                            <a href="#" class="btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill" data-toggle="dropdown" aria-expanded="true">
                              <i class="la la-ellipsis-h"></i>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right">
                                <a class="dropdown-item" href="#"><i class="la la-edit"></i> Edit Details</a>
                                <a class="dropdown-item" href="#"><i class="la la-leaf"></i> Apporval</a>
                                <a class="dropdown-item" href="#"><i class="la la-print"></i> Rejected</a>
                            </div>
                        </span>
                        <a href="#" class="m-portlet__nav-link btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill" title="View">
                          <i class="la la-edit"></i>
                        </a></td></tr><tr role="row" class="odd"><td class="sorting_1" tabindex="0">1</td><td>Name1</td><td>read</td><td>Staff</td><td>Doctor</td><td>2/12/2018</td><td><span class="m-badge  m-badge--primary m-badge--wide">Apporved</span></td><td><span class="m-badge m-badge--danger m-badge--dot"></span>&nbsp;<span class="m--font-bold m--font-danger">1/12/2019</span></td>
                        <td>
                        <span class="dropdown">
                            <a href="#" class="btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill" data-toggle="dropdown" aria-expanded="true">
                              <i class="la la-ellipsis-h"></i>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right">
                                <a class="dropdown-item" href="#"><i class="la la-edit"></i> Edit Details</a>
                                <a class="dropdown-item" href="#"><i class="la la-leaf"></i> Apporval</a>
                                <a class="dropdown-item" href="#"><i class="la la-print"></i> Rejected</a>
                            </div>
                        </span>
                        <a href="#" class="m-portlet__nav-link btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill" title="View">
                          <i class="la la-edit"></i>
                        </a></td></tr><tr role="row" class="even"><td class="sorting_1" tabindex="0">1</td><td>Name1</td><td>read</td><td>Staff</td><td>Doctor</td><td>2/12/2018</td><td><span class="m-badge  m-badge--primary m-badge--wide">Apporved</span></td><td><span class="m-badge m-badge--danger m-badge--dot"></span>&nbsp;<span class="m--font-bold m--font-danger">1/12/2019</span></td>
                        <td>
                        <span class="dropdown">
                            <a href="#" class="btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill" data-toggle="dropdown" aria-expanded="true">
                              <i class="la la-ellipsis-h"></i>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right">
                                <a class="dropdown-item" href="#"><i class="la la-edit"></i> Edit Details</a>
                                <a class="dropdown-item" href="#"><i class="la la-leaf"></i> Apporval</a>
                                <a class="dropdown-item" href="#"><i class="la la-print"></i> Rejected</a>
                            </div>
                        </span>
                        <a href="#" class="m-portlet__nav-link btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill" title="View">
                          <i class="la la-edit"></i>
                        </a></td></tr><tr role="row" class="odd"><td class="sorting_1" tabindex="0">1</td><td>Name1</td><td>read</td><td>Staff</td><td>Doctor</td><td>2/12/2018</td><td><span class="m-badge  m-badge--primary m-badge--wide">Apporved</span></td><td><span class="m-badge m-badge--danger m-badge--dot"></span>&nbsp;<span class="m--font-bold m--font-danger">1/12/2019</span></td>
                        <td>
                        <span class="dropdown">
                            <a href="#" class="btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill" data-toggle="dropdown" aria-expanded="true">
                              <i class="la la-ellipsis-h"></i>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right">
                                <a class="dropdown-item" href="#"><i class="la la-edit"></i> Edit Details</a>
                                <a class="dropdown-item" href="#"><i class="la la-leaf"></i> Apporval</a>
                                <a class="dropdown-item" href="#"><i class="la la-print"></i> Rejected</a>
                            </div>
                        </span>
                        <a href="#" class="m-portlet__nav-link btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill" title="View">
                          <i class="la la-edit"></i>
                        </a></td></tr><tr role="row" class="even"><td class="sorting_1" tabindex="0">1</td><td>Name1</td><td>read</td><td>Staff</td><td>Doctor</td><td>2/12/2018</td><td><span class="m-badge  m-badge--primary m-badge--wide">Apporved</span></td><td><span class="m-badge m-badge--danger m-badge--dot"></span>&nbsp;<span class="m--font-bold m--font-danger">1/12/2019</span></td>
                        <td>
                        <span class="dropdown">
                            <a href="#" class="btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill" data-toggle="dropdown" aria-expanded="true">
                              <i class="la la-ellipsis-h"></i>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right">
                                <a class="dropdown-item" href="#"><i class="la la-edit"></i> Edit Details</a>
                                <a class="dropdown-item" href="#"><i class="la la-leaf"></i> Apporval</a>
                                <a class="dropdown-item" href="#"><i class="la la-print"></i> Rejected</a>
                            </div>
                        </span>
                        <a href="#" class="m-portlet__nav-link btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill" title="View">
                          <i class="la la-edit"></i>
                        </a></td></tr></tbody></table><div id="m_table_1_processing" class="dataTables_processing card" style="display: none;">Processing...</div></div></div><div class="row"><div class="col-sm-12 col-md-5"><div class="dataTables_info" id="m_table_1_info" role="status" aria-live="polite">Showing 1 to 10 of 350 entries</div></div><div class="col-sm-12 col-md-7 dataTables_pager"><div class="dataTables_length" id="m_table_1_length"><label>Display <select name="m_table_1_length" aria-controls="m_table_1" class="custom-select custom-select-sm form-control form-control-sm"><option value="5">5</option><option value="10">10</option><option value="25">25</option><option value="50">50</option></select></label></div><div class="dataTables_paginate paging_simple_numbers" id="m_table_1_paginate"><ul class="pagination"><li class="paginate_button page-item previous disabled" id="m_table_1_previous"><a href="#" aria-controls="m_table_1" data-dt-idx="0" tabindex="0" class="page-link"><i class="la la-angle-left"></i></a></li><li class="paginate_button page-item active"><a href="#" aria-controls="m_table_1" data-dt-idx="1" tabindex="0" class="page-link">1</a></li><li class="paginate_button page-item "><a href="#" aria-controls="m_table_1" data-dt-idx="2" tabindex="0" class="page-link">2</a></li><li class="paginate_button page-item "><a href="#" aria-controls="m_table_1" data-dt-idx="3" tabindex="0" class="page-link">3</a></li><li class="paginate_button page-item "><a href="#" aria-controls="m_table_1" data-dt-idx="4" tabindex="0" class="page-link">4</a></li><li class="paginate_button page-item "><a href="#" aria-controls="m_table_1" data-dt-idx="5" tabindex="0" class="page-link">5</a></li><li class="paginate_button page-item disabled" id="m_table_1_ellipsis"><a href="#" aria-controls="m_table_1" data-dt-idx="6" tabindex="0" class="page-link">…</a></li><li class="paginate_button page-item "><a href="#" aria-controls="m_table_1" data-dt-idx="7" tabindex="0" class="page-link">35</a></li><li class="paginate_button page-item next" id="m_table_1_next"><a href="#" aria-controls="m_table_1" data-dt-idx="8" tabindex="0" class="page-link"><i class="la la-angle-right"></i></a></li></ul></div></div></div></div>
	</div>
</div>
          <!-- /.box-body -->
        </div>                     </div>
                     
                  </div>
                  <!--End::Section-->
                  <!--- begin section --->
               
                   
                  <!--Begin::Section-->
                  <div class="row">
                     <div class="col-xl-4">
                        <!--begin:: Widgets/Finance Summary-->
                        <!--<div class="m-portlet m-portlet--full-height ">
                           <div class="m-portlet__head">
                              <div class="m-portlet__head-caption">
                                 <div class="m-portlet__head-title">
                                    <h3 class="m-portlet__head-text">
                                       Finance Summary
                                    </h3>
                                 </div>
                              </div>
                              <div class="m-portlet__head-tools">
                                 <ul class="m-portlet__nav">
                                    <li class="m-portlet__nav-item m-dropdown m-dropdown--inline m-dropdown--arrow m-dropdown--align-right m-dropdown--align-push" m-dropdown-toggle="hover" aria-expanded="true">
                                       <a href="#" class="m-portlet__nav-link m-dropdown__toggle dropdown-toggle btn btn--sm m-btn--pill btn-secondary m-btn m-btn--label-danger">
                                       Today
                                       </a>
                                       <div class="m-dropdown__wrapper">
                                          <span class="m-dropdown__arrow m-dropdown__arrow--right m-dropdown__arrow--adjust" style="left: auto; right: 34.5px;"></span>
                                          <div class="m-dropdown__inner">
                                             <div class="m-dropdown__body">
                                                <div class="m-dropdown__content">
                                                   <ul class="m-nav">
                                                      <li class="m-nav__section m-nav__section--first">
                                                         <span class="m-nav__section-text">Quick Actions</span>
                                                      </li>
                                                      <li class="m-nav__item">
                                                         <a href="" class="m-nav__link">
                                                         <i class="m-nav__link-icon flaticon-share"></i>
                                                         <span class="m-nav__link-text">Activity</span>
                                                         </a>
                                                      </li>
                                                      <li class="m-nav__item">
                                                         <a href="" class="m-nav__link">
                                                         <i class="m-nav__link-icon flaticon-chat-1"></i>
                                                         <span class="m-nav__link-text">Messages</span>
                                                         </a>
                                                      </li>
                                                      <li class="m-nav__item">
                                                         <a href="" class="m-nav__link">
                                                         <i class="m-nav__link-icon flaticon-info"></i>
                                                         <span class="m-nav__link-text">FAQ</span>
                                                         </a>
                                                      </li>
                                                      <li class="m-nav__item">
                                                         <a href="" class="m-nav__link">
                                                         <i class="m-nav__link-icon flaticon-lifebuoy"></i>
                                                         <span class="m-nav__link-text">Support</span>
                                                         </a>
                                                      </li>
                                                      <li class="m-nav__separator m-nav__separator--fit"></li>
                                                      <li class="m-nav__item">
                                                         <a href="#" class="btn btn-outline-danger m-btn m-btn--pill m-btn--wide btn-sm">Cancel</a>
                                                      </li>
                                                   </ul>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </li>
                                 </ul>
                              </div>
                           </div>
                           <div class="m-portlet__body">
                              <div class="m-widget12">
                                 <div class="m-widget12__item">
                                    <span class="m-widget12__text1">Annual Companies Taxes EMS
                                    <br>
                                    <span>$500,000</span>
                                    </span>
                                    <span class="m-widget12__text2">Next Tax Review Date
                                    <br>
                                    <span>July 24,2017</span>
                                    </span>
                                 </div>
                                 <div class="m-widget12__item">
                                    <span class="m-widget12__text1">Total Annual Profit Before Tax
                                    <br>
                                    <span>$3,800,000</span>
                                    </span>
                                    <span class="m-widget12__text2">Type Of Market Share
                                    <br>
                                    <span>Grossery</span>
                                    </span>
                                 </div>
                                 <div class="m-widget12__item">
                                    <span class="m-widget12__text1">Avarage Product Price
                                    <br>
                                    <span>$60,70</span>
                                    </span>
                                    <div class="m-widget12__text2">
                                       <div class="m-widget12__desc">Satisfication Rate</div>
                                       <br>
                                       <div class="m-widget12__progress">
                                          <div class="m-widget12__progress-sm progress m-progress--sm">
                                             <div class="m-widget12__progress-bar progress-bar bg-brand" role="progressbar" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                          </div>
                                          <span class="m-widget12__stats">
                                          63%
                                          </span>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>-->
                         
                         
                         <div class="m-portlet m-portlet--bordered-semi m-portlet--widget-fit m-portlet--full-height m-portlet--skin-light  m-portlet--rounded-force">
	<div class="m-portlet__head">
		<div class="m-portlet__head-caption">
			<div class="m-portlet__head-title">
				<h3 class="m-portlet__head-text m--font-light">
					Activity
				</h3>
			</div>
		</div>
		<div class="m-portlet__head-tools">
			<ul class="m-portlet__nav">
				<li class="m-portlet__nav-item m-dropdown m-dropdown--inline m-dropdown--arrow m-dropdown--align-right m-dropdown--align-push" m-dropdown-toggle="hover">
					<a href="#" class="m-portlet__nav-link m-portlet__nav-link--icon m-portlet__nav-link--icon-xl">
						<i class="fa fa-genderless m--font-light"></i>
					</a>
					<div class="m-dropdown__wrapper">
						<span class="m-dropdown__arrow m-dropdown__arrow--right m-dropdown__arrow--adjust"></span>
						<div class="m-dropdown__inner">
							<div class="m-dropdown__body">
								<div class="m-dropdown__content">
									<ul class="m-nav">
										<li class="m-nav__section m-nav__section--first">
											<span class="m-nav__section-text">Quick Actions</span>
										</li>
										<li class="m-nav__item">
											<a href="" class="m-nav__link">
											<i class="m-nav__link-icon flaticon-share"></i>
											<span class="m-nav__link-text">Activity</span>
											</a>
										</li>
										<li class="m-nav__item">
											<a href="" class="m-nav__link">
											<i class="m-nav__link-icon flaticon-chat-1"></i>
											<span class="m-nav__link-text">Messages</span>
											</a>
										</li>
										<li class="m-nav__item">
											<a href="" class="m-nav__link">
											<i class="m-nav__link-icon flaticon-info"></i>
											<span class="m-nav__link-text">FAQ</span>
											</a>
										</li>
										<li class="m-nav__item">
											<a href="" class="m-nav__link">
											<i class="m-nav__link-icon flaticon-lifebuoy"></i>
											<span class="m-nav__link-text">Support</span>
											</a>
										</li>
										<li class="m-nav__separator m-nav__separator--fit">
										</li>
										<li class="m-nav__item">
											<a href="#" class="btn btn-outline-danger m-btn m-btn--pill m-btn--wide btn-sm">Cancel</a>
										</li>
									</ul>
								</div>
							</div>
						</div>
					</div>
				</li>
			</ul>
		</div>
	</div>
	<div class="m-portlet__body">
		<div class="m-widget17">
			<div class="m-widget17__visual m-widget17__visual--chart m-portlet-fit--top m-portlet-fit--sides m--bg-danger">
				<div class="m-widget17__chart" style="height:253px;"><div class="chartjs-size-monitor" style="position: absolute; left: 0px; top: 0px; right: 0px; bottom: 0px; overflow: hidden; pointer-events: none; visibility: hidden; z-index: -1;"><div class="chartjs-size-monitor-expand" style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;"><div style="position:absolute;width:1000000px;height:1000000px;left:0;top:0"></div></div><div class="chartjs-size-monitor-shrink" style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;"><div style="position:absolute;width:200%;height:200%;left:0; top:0"></div></div></div>
					<canvas id="m_chart_activities" width="325" height="216" class="chartjs-render-monitor" style="display: block; width: 325px; height: 216px;"></canvas>
				</div>
			</div>
			<div class="m-widget17__stats">
				<div class="m-widget17__items m-widget17__items-col1">
					<div class="m-widget17__item">
						<span class="m-widget17__icon">
							<i class="flaticon-user m--font-brand"></i>             
						</span> 
						<span class="m-widget17__subtitle">
							Registered Users
						</span> 
						<span class="m-widget17__desc">
							715 Pesrsons
						</span>  
					</div>
					<div class="m-widget17__item">
						<span class="m-widget17__icon">
							<i class="flaticon-doc m--font-info"></i>             
						</span>  
						<span class="m-widget17__subtitle">
							Total Documents
						</span> 
						<span class="m-widget17__desc">
							672 Documents
						</span>  
					</div>
				</div>
				<div class="m-widget17__items m-widget17__items-col2">			     
					<div class="m-widget17__item">
						<span class="m-widget17__icon">
							<i class="flaticon-arrows m--font-success"></i>
						</span>  
						<span class="m-widget17__subtitle">
							Pending Documents
						</span> 
						<span class="m-widget17__desc">
							72 Documents
						</span>  
					</div>
					<div class="m-widget17__item">
						<span class="m-widget17__icon">
							<i class="flaticon-circle m--font-danger"></i>
						</span>  
						<span class="m-widget17__subtitle">
							Rejected Documents
						</span> 
						<span class="m-widget17__desc">
							34 Documents
						</span>  
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
                         
                        <!--end:: Widgets/Finance Summary-->
                     </div>
                     <div class="col-xl-4">
                         
                         <div class="m-widget14" style="background:#fff;padding: 6.6rem 2.2rem;">
	<div class="m-widget14__header">
		<h3 class="m-widget14__title">
			Uploads      
		</h3>
		<span class="m-widget14__desc" style="margin-bottom: 6em;">
		Category wise documents Uploads
		</span>
	</div>
	<div class="row  align-items-center">
		<div class="col">
			<div id="m_chart_profit_share" class="m-widget14__chart" style="height: 160px">
				<div class="m-widget14__stat">45</div>
			<svg xmlns:ct="http://gionkunz.github.com/chartist-js/ct" width="100%" height="100%" class="ct-chart-donut" style="width: 100%; height: 100%; display:none;"><g class="ct-series custom"><path d="M110.393,101.658A50.867,50.867,0,0,0,64.367,29.133" class="ct-slice-donut" ct:value="32" ct:meta="{&amp;quot;data&amp;quot;:{&amp;quot;color&amp;quot;:&amp;quot;#716aca&amp;quot;}}" style="stroke-width: 17px;" stroke-dasharray="102.27520751953125px 102.27520751953125px" stroke-dashoffset="-102.27520751953125px" stroke="#716aca"><animate attributeName="stroke-dashoffset" id="anim0" dur="1000ms" from="-102.27520751953125px" to="0px" fill="freeze" stroke="#716aca" calcMode="spline" keySplines="0.23 1 0.32 1" keyTimes="0;1"></animate></path></g><g class="ct-series custom"><path d="M25.173,112.424A50.867,50.867,0,0,0,110.469,101.497" class="ct-slice-donut" ct:value="32" ct:meta="{&amp;quot;data&amp;quot;:{&amp;quot;color&amp;quot;:&amp;quot;#00c5dc&amp;quot;}}" style="stroke-width: 17px;" stroke-dasharray="102.45503234863281px 102.45503234863281px" stroke-dashoffset="-102.45503234863281px" stroke="#00c5dc"><animate attributeName="stroke-dashoffset" id="anim1" dur="1000ms" from="-102.45503234863281px" to="0px" fill="freeze" stroke="#00c5dc" begin="anim0.end" calcMode="spline" keySplines="0.23 1 0.32 1" keyTimes="0;1"></animate></path></g><g class="ct-series custom"><path d="M64.367,29.133A50.867,50.867,0,0,0,25.287,112.561" class="ct-slice-donut" ct:value="36" ct:meta="{&amp;quot;data&amp;quot;:{&amp;quot;color&amp;quot;:&amp;quot;#ffb822&amp;quot;}}" style="stroke-width: 17px;" stroke-dasharray="115.23920440673828px 115.23920440673828px" stroke-dashoffset="-115.23920440673828px" stroke="#ffb822"><animate attributeName="stroke-dashoffset" id="anim2" dur="1000ms" from="-115.23920440673828px" to="0px" fill="freeze" stroke="#ffb822" begin="anim1.end" calcMode="spline" keySplines="0.23 1 0.32 1" keyTimes="0;1"></animate></path></g></svg></div>
		</div>
		<div class="col">
			<div class="m-widget14__legends">
				<div class="m-widget14__legend">
					<span class="m-widget14__legend-bullet m--bg-accent"></span>
					<span class="m-widget14__legend-text">37% Admin Documents</span>
				</div>
				<div class="m-widget14__legend">
					<span class="m-widget14__legend-bullet m--bg-warning"></span>
					<span class="m-widget14__legend-text">47% Practice Documents</span>
				</div>
				<div class="m-widget14__legend">
					<span class="m-widget14__legend-bullet m--bg-brand"></span>
					<span class="m-widget14__legend-text">19% Employes Documents</span>
				</div>
			</div>
		</div>
	</div>
</div>
                     
                        <!--end:: Widgets/Sale Reports-->
                     </div>
                     <div class="col-xl-4">
				<!--begin:: Widgets/Revenue Change-->

<!--end:: Widgets/Revenue Change-->			</div>
                  </div>
                  <!--End::Section-->
                  <!--Begin::Section-->
                  
                  <!--End::Section-->
                  <!--Begin::Section-->
                  
                  <!--End::Section-->
                  <!--Begin::Section-->
                  
                  <!--End::Section-->
               </div>
            </div>
         </div>
         <!-- end:: Body -->
         <!-- begin::Footer -->
         @include('layout.footer')
         <!-- end::Footer -->
      </div>
      <!-- end:: Page -->
      <!-- begin::Quick Sidebar -->
      <div id="m_quick_sidebar" class="m-quick-sidebar m-quick-sidebar--tabbed m-quick-sidebar--skin-light">
         <div class="m-quick-sidebar__content m--hide">
            <span id="m_quick_sidebar_close" class="m-quick-sidebar__close">
            <i class="la la-close"></i>
            </span>
            <ul id="m_quick_sidebar_tabs" class="nav nav-tabs m-tabs m-tabs-line m-tabs-line--brand" role="tablist">
               <li class="nav-item m-tabs__item">
                  <a class="nav-link m-tabs__link active" data-toggle="tab" href="#m_quick_sidebar_tabs_messenger" role="tab">Messages</a>
               </li>
               <li class="nav-item m-tabs__item">
                  <a class="nav-link m-tabs__link" data-toggle="tab" href="#m_quick_sidebar_tabs_settings" role="tab">Settings</a>
               </li>
               <li class="nav-item m-tabs__item">
                  <a class="nav-link m-tabs__link" data-toggle="tab" href="#m_quick_sidebar_tabs_logs" role="tab">Logs</a>
               </li>
            </ul>
            <div class="tab-content">
               <div class="tab-pane active" id="m_quick_sidebar_tabs_messenger" role="tabpanel">
                  <div class="m-messenger m-messenger--message-arrow m-messenger--skin-light">
                     <div class="m-messenger__messages m-scrollable">
                        <div class="m-messenger__wrapper">
                           <div class="m-messenger__message m-messenger__message--in">
                              <div class="m-messenger__message-pic">
                                 <img src="assets/admin/assets/app/media/img/users/user3.jpg" alt="" />
                              </div>
                              <div class="m-messenger__message-body">
                                 <div class="m-messenger__message-arrow"></div>
                                 <div class="m-messenger__message-content">
                                    <div class="m-messenger__message-username">
                                       Megan wrote
                                    </div>
                                    <div class="m-messenger__message-text">
                                       Hi Bob. What time will be the meeting ?
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="m-messenger__wrapper">
                           <div class="m-messenger__message m-messenger__message--out">
                              <div class="m-messenger__message-body">
                                 <div class="m-messenger__message-arrow"></div>
                                 <div class="m-messenger__message-content">
                                    <div class="m-messenger__message-text">
                                       Hi Megan. It's at 2.30PM
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="m-messenger__wrapper">
                           <div class="m-messenger__message m-messenger__message--in">
                              <div class="m-messenger__message-pic">
                                 <img src="assets/admin/assets/app/media/img//users/user3.jpg" alt="" />
                              </div>
                              <div class="m-messenger__message-body">
                                 <div class="m-messenger__message-arrow"></div>
                                 <div class="m-messenger__message-content">
                                    <div class="m-messenger__message-username">
                                       Megan wrote
                                    </div>
                                    <div class="m-messenger__message-text">
                                       Will the development team be joining ?
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="m-messenger__wrapper">
                           <div class="m-messenger__message m-messenger__message--out">
                              <div class="m-messenger__message-body">
                                 <div class="m-messenger__message-arrow"></div>
                                 <div class="m-messenger__message-content">
                                    <div class="m-messenger__message-text">
                                       Yes sure. I invited them as well
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="m-messenger__datetime">2:30PM</div>
                        <div class="m-messenger__wrapper">
                           <div class="m-messenger__message m-messenger__message--in">
                              <div class="m-messenger__message-pic">
                                 <img src="assets/admin/assets/app/media/img//users/user3.jpg" alt="" />
                              </div>
                              <div class="m-messenger__message-body">
                                 <div class="m-messenger__message-arrow"></div>
                                 <div class="m-messenger__message-content">
                                    <div class="m-messenger__message-username">
                                       Megan wrote
                                    </div>
                                    <div class="m-messenger__message-text">
                                       Noted. For the Coca-Cola Mobile App project as well ?
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="m-messenger__wrapper">
                           <div class="m-messenger__message m-messenger__message--out">
                              <div class="m-messenger__message-body">
                                 <div class="m-messenger__message-arrow"></div>
                                 <div class="m-messenger__message-content">
                                    <div class="m-messenger__message-text">
                                       Yes, sure.
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="m-messenger__wrapper">
                           <div class="m-messenger__message m-messenger__message--out">
                              <div class="m-messenger__message-body">
                                 <div class="m-messenger__message-arrow"></div>
                                 <div class="m-messenger__message-content">
                                    <div class="m-messenger__message-text">
                                       Please also prepare the quotation for the Loop CRM project as well.
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="m-messenger__datetime">3:15PM</div>
                        <div class="m-messenger__wrapper">
                           <div class="m-messenger__message m-messenger__message--in">
                              <div class="m-messenger__message-no-pic m--bg-fill-danger">
                                 <span>M</span>
                              </div>
                              <div class="m-messenger__message-body">
                                 <div class="m-messenger__message-arrow"></div>
                                 <div class="m-messenger__message-content">
                                    <div class="m-messenger__message-username">
                                       Megan wrote
                                    </div>
                                    <div class="m-messenger__message-text">
                                       Noted. I will prepare it.
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="m-messenger__wrapper">
                           <div class="m-messenger__message m-messenger__message--out">
                              <div class="m-messenger__message-body">
                                 <div class="m-messenger__message-arrow"></div>
                                 <div class="m-messenger__message-content">
                                    <div class="m-messenger__message-text">
                                       Thanks Megan. I will see you later.
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="m-messenger__wrapper">
                           <div class="m-messenger__message m-messenger__message--in">
                              <div class="m-messenger__message-pic">
                                 <img src="assets/app/media/img//users/user3.jpg" alt="" />
                              </div>
                              <div class="m-messenger__message-body">
                                 <div class="m-messenger__message-arrow"></div>
                                 <div class="m-messenger__message-content">
                                    <div class="m-messenger__message-username">
                                       Megan wrote
                                    </div>
                                    <div class="m-messenger__message-text">
                                       Sure. See you in the meeting soon.
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="m-messenger__seperator"></div>
                     <div class="m-messenger__form">
                        <div class="m-messenger__form-controls">
                           <input type="text" name="" placeholder="Type here..." class="m-messenger__form-input">
                        </div>
                        <div class="m-messenger__form-tools">
                           <a href="" class="m-messenger__form-attachment">
                           <i class="la la-paperclip"></i>
                           </a>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="tab-pane" id="m_quick_sidebar_tabs_settings" role="tabpanel">
                  <div class="m-list-settings m-scrollable">
                     <div class="m-list-settings__group">
                        <div class="m-list-settings__heading">General Settings</div>
                        <div class="m-list-settings__item">
                           <span class="m-list-settings__item-label">Email Notifications</span>
                           <span class="m-list-settings__item-control">
                           <span class="m-switch m-switch--outline m-switch--icon-check m-switch--brand">
                           <label>
                           <input type="checkbox" checked="checked" name="">
                           <span></span>
                           </label>
                           </span>
                           </span>
                        </div>
                        <div class="m-list-settings__item">
                           <span class="m-list-settings__item-label">Site Tracking</span>
                           <span class="m-list-settings__item-control">
                           <span class="m-switch m-switch--outline m-switch--icon-check m-switch--brand">
                           <label>
                           <input type="checkbox" name="">
                           <span></span>
                           </label>
                           </span>
                           </span>
                        </div>
                        <div class="m-list-settings__item">
                           <span class="m-list-settings__item-label">SMS Alerts</span>
                           <span class="m-list-settings__item-control">
                           <span class="m-switch m-switch--outline m-switch--icon-check m-switch--brand">
                           <label>
                           <input type="checkbox" name="">
                           <span></span>
                           </label>
                           </span>
                           </span>
                        </div>
                        <div class="m-list-settings__item">
                           <span class="m-list-settings__item-label">Backup Storage</span>
                           <span class="m-list-settings__item-control">
                           <span class="m-switch m-switch--outline m-switch--icon-check m-switch--brand">
                           <label>
                           <input type="checkbox" name="">
                           <span></span>
                           </label>
                           </span>
                           </span>
                        </div>
                        <div class="m-list-settings__item">
                           <span class="m-list-settings__item-label">Audit Logs</span>
                           <span class="m-list-settings__item-control">
                           <span class="m-switch m-switch--outline m-switch--icon-check m-switch--brand">
                           <label>
                           <input type="checkbox" checked="checked" name="">
                           <span></span>
                           </label>
                           </span>
                           </span>
                        </div>
                     </div>
                     <div class="m-list-settings__group">
                        <div class="m-list-settings__heading">System Settings</div>
                        <div class="m-list-settings__item">
                           <span class="m-list-settings__item-label">System Logs</span>
                           <span class="m-list-settings__item-control">
                           <span class="m-switch m-switch--outline m-switch--icon-check m-switch--brand">
                           <label>
                           <input type="checkbox" name="">
                           <span></span>
                           </label>
                           </span>
                           </span>
                        </div>
                        <div class="m-list-settings__item">
                           <span class="m-list-settings__item-label">Error Reporting</span>
                           <span class="m-list-settings__item-control">
                           <span class="m-switch m-switch--outline m-switch--icon-check m-switch--brand">
                           <label>
                           <input type="checkbox" name="">
                           <span></span>
                           </label>
                           </span>
                           </span>
                        </div>
                        <div class="m-list-settings__item">
                           <span class="m-list-settings__item-label">Applications Logs</span>
                           <span class="m-list-settings__item-control">
                           <span class="m-switch m-switch--outline m-switch--icon-check m-switch--brand">
                           <label>
                           <input type="checkbox" name="">
                           <span></span>
                           </label>
                           </span>
                           </span>
                        </div>
                        <div class="m-list-settings__item">
                           <span class="m-list-settings__item-label">Backup Servers</span>
                           <span class="m-list-settings__item-control">
                           <span class="m-switch m-switch--outline m-switch--icon-check m-switch--brand">
                           <label>
                           <input type="checkbox" checked="checked" name="">
                           <span></span>
                           </label>
                           </span>
                           </span>
                        </div>
                        <div class="m-list-settings__item">
                           <span class="m-list-settings__item-label">Audit Logs</span>
                           <span class="m-list-settings__item-control">
                           <span class="m-switch m-switch--outline m-switch--icon-check m-switch--brand">
                           <label>
                           <input type="checkbox" name="">
                           <span></span>
                           </label>
                           </span>
                           </span>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="tab-pane" id="m_quick_sidebar_tabs_logs" role="tabpanel">
                  <div class="m-list-timeline m-scrollable">
                     <div class="m-list-timeline__group">
                        <div class="m-list-timeline__heading">
                           System Logs
                        </div>
                        <div class="m-list-timeline__items">
                           <div class="m-list-timeline__item">
                              <span class="m-list-timeline__badge m-list-timeline__badge--state-success"></span>
                              <a href="" class="m-list-timeline__text">12 new users registered 
                              <span class="m-badge m-badge--warning m-badge--wide">important</span>
                              </a>
                              <span class="m-list-timeline__time">Just now</span>
                           </div>
                           <div class="m-list-timeline__item">
                              <span class="m-list-timeline__badge m-list-timeline__badge--state-info"></span>
                              <a href="" class="m-list-timeline__text">System shutdown</a>
                              <span class="m-list-timeline__time">11 mins</span>
                           </div>
                           <div class="m-list-timeline__item">
                              <span class="m-list-timeline__badge m-list-timeline__badge--state-danger"></span>
                              <a href="" class="m-list-timeline__text">New invoice received</a>
                              <span class="m-list-timeline__time">20 mins</span>
                           </div>
                           <div class="m-list-timeline__item">
                              <span class="m-list-timeline__badge m-list-timeline__badge--state-warning"></span>
                              <a href="" class="m-list-timeline__text">Database overloaded 89% 
                              <span class="m-badge m-badge--success m-badge--wide">resolved</span>
                              </a>
                              <span class="m-list-timeline__time">1 hr</span>
                           </div>
                           <div class="m-list-timeline__item">
                              <span class="m-list-timeline__badge m-list-timeline__badge--state-success"></span>
                              <a href="" class="m-list-timeline__text">System error</a>
                              <span class="m-list-timeline__time">2 hrs</span>
                           </div>
                           <div class="m-list-timeline__item">
                              <span class="m-list-timeline__badge m-list-timeline__badge--state-info"></span>
                              <a href="" class="m-list-timeline__text">Production server down 
                              <span class="m-badge m-badge--danger m-badge--wide">pending</span>
                              </a>
                              <span class="m-list-timeline__time">3 hrs</span>
                           </div>
                           <div class="m-list-timeline__item">
                              <span class="m-list-timeline__badge m-list-timeline__badge--state-success"></span>
                              <a href="" class="m-list-timeline__text">Production server up</a>
                              <span class="m-list-timeline__time">5 hrs</span>
                           </div>
                        </div>
                     </div>
                     <div class="m-list-timeline__group">
                        <div class="m-list-timeline__heading">
                           Applications Logs
                        </div>
                        <div class="m-list-timeline__items">
                           <div class="m-list-timeline__item">
                              <span class="m-list-timeline__badge m-list-timeline__badge--state-info"></span>
                              <a href="" class="m-list-timeline__text">New order received 
                              <span class="m-badge m-badge--info m-badge--wide">urgent</span>
                              </a>
                              <span class="m-list-timeline__time">7 hrs</span>
                           </div>
                           <div class="m-list-timeline__item">
                              <span class="m-list-timeline__badge m-list-timeline__badge--state-success"></span>
                              <a href="" class="m-list-timeline__text">12 new users registered</a>
                              <span class="m-list-timeline__time">Just now</span>
                           </div>
                           <div class="m-list-timeline__item">
                              <span class="m-list-timeline__badge m-list-timeline__badge--state-info"></span>
                              <a href="" class="m-list-timeline__text">System shutdown</a>
                              <span class="m-list-timeline__time">11 mins</span>
                           </div>
                           <div class="m-list-timeline__item">
                              <span class="m-list-timeline__badge m-list-timeline__badge--state-danger"></span>
                              <a href="" class="m-list-timeline__text">New invoices received</a>
                              <span class="m-list-timeline__time">20 mins</span>
                           </div>
                           <div class="m-list-timeline__item">
                              <span class="m-list-timeline__badge m-list-timeline__badge--state-warning"></span>
                              <a href="" class="m-list-timeline__text">Database overloaded 89%</a>
                              <span class="m-list-timeline__time">1 hr</span>
                           </div>
                           <div class="m-list-timeline__item">
                              <span class="m-list-timeline__badge m-list-timeline__badge--state-success"></span>
                              <a href="" class="m-list-timeline__text">System error 
                              <span class="m-badge m-badge--info m-badge--wide">pending</span>
                              </a>
                              <span class="m-list-timeline__time">2 hrs</span>
                           </div>
                           <div class="m-list-timeline__item">
                              <span class="m-list-timeline__badge m-list-timeline__badge--state-info"></span>
                              <a href="" class="m-list-timeline__text">Production server down</a>
                              <span class="m-list-timeline__time">3 hrs</span>
                           </div>
                        </div>
                     </div>
                     <div class="m-list-timeline__group">
                        <div class="m-list-timeline__heading">
                           Server Logs
                        </div>
                        <div class="m-list-timeline__items">
                           <div class="m-list-timeline__item">
                              <span class="m-list-timeline__badge m-list-timeline__badge--state-success"></span>
                              <a href="" class="m-list-timeline__text">Production server up</a>
                              <span class="m-list-timeline__time">5 hrs</span>
                           </div>
                           <div class="m-list-timeline__item">
                              <span class="m-list-timeline__badge m-list-timeline__badge--state-info"></span>
                              <a href="" class="m-list-timeline__text">New order received</a>
                              <span class="m-list-timeline__time">7 hrs</span>
                           </div>
                           <div class="m-list-timeline__item">
                              <span class="m-list-timeline__badge m-list-timeline__badge--state-success"></span>
                              <a href="" class="m-list-timeline__text">12 new users registered</a>
                              <span class="m-list-timeline__time">Just now</span>
                           </div>
                           <div class="m-list-timeline__item">
                              <span class="m-list-timeline__badge m-list-timeline__badge--state-info"></span>
                              <a href="" class="m-list-timeline__text">System shutdown</a>
                              <span class="m-list-timeline__time">11 mins</span>
                           </div>
                           <div class="m-list-timeline__item">
                              <span class="m-list-timeline__badge m-list-timeline__badge--state-danger"></span>
                              <a href="" class="m-list-timeline__text">New invoice received</a>
                              <span class="m-list-timeline__time">20 mins</span>
                           </div>
                           <div class="m-list-timeline__item">
                              <span class="m-list-timeline__badge m-list-timeline__badge--state-warning"></span>
                              <a href="" class="m-list-timeline__text">Database overloaded 89%</a>
                              <span class="m-list-timeline__time">1 hr</span>
                           </div>
                           <div class="m-list-timeline__item">
                              <span class="m-list-timeline__badge m-list-timeline__badge--state-success"></span>
                              <a href="" class="m-list-timeline__text">System error</a>
                              <span class="m-list-timeline__time">2 hrs</span>
                           </div>
                           <div class="m-list-timeline__item">
                              <span class="m-list-timeline__badge m-list-timeline__badge--state-info"></span>
                              <a href="" class="m-list-timeline__text">Production server down</a>
                              <span class="m-list-timeline__time">3 hrs</span>
                           </div>
                           <div class="m-list-timeline__item">
                              <span class="m-list-timeline__badge m-list-timeline__badge--state-success"></span>
                              <a href="" class="m-list-timeline__text">Production server up</a>
                              <span class="m-list-timeline__time">5 hrs</span>
                           </div>
                           <div class="m-list-timeline__item">
                              <span class="m-list-timeline__badge m-list-timeline__badge--state-info"></span>
                              <a href="" class="m-list-timeline__text">New order received</a>
                              <span class="m-list-timeline__time">1117 hrs</span>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- end::Quick Sidebar -->
      <!-- begin::Scroll Top -->
      <div id="m_scroll_top" class="m-scroll-top">
         <i class="la la-arrow-up"></i>
      </div>
      <!-- end::Scroll Top -->
      <!-- begin::Quick Nav -->
      <!--<ul class="m-nav-sticky" style="margin-top: 30px;">
         <li class="m-nav-sticky__item" data-toggle="m-tooltip" title="Purchase" data-placement="left">
            <a href="https://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes" target="_blank">
            <i class="la la-cart-arrow-down"></i>
            </a>
         </li>
         <li class="m-nav-sticky__item" data-toggle="m-tooltip" title="Documentation" data-placement="left">
            <a href="https://keenthemes.com/metronic/documentation.html" target="_blank">
            <i class="la la-code-fork"></i>
            </a>
         </li>
         <li class="m-nav-sticky__item" data-toggle="m-tooltip" title="Support" data-placement="left">
            <a href="https://keenthemes.com/forums/forum/support/metronic5/" target="_blank">
            <i class="la la-life-ring"></i>
            </a>
         </li>
      </ul>-->
      <!-- begin::Quick Nav -->
      <!--begin::Global Theme Bundle -->
      <script src="assets/admin/assets/vendors/base/vendors.bundle.js" type="text/javascript"></script>
      <script src="assets/admin/assets/demo/demo3/base/scripts.bundle.js" type="text/javascript"></script>
      <!--end::Global Theme Bundle -->
      <!--begin::Page Vendors -->
      <script src="assets/admin/assets/vendors/custom/fullcalendar/fullcalendar.bundle.js" type="text/javascript"></script>
      <!--end::Page Vendors -->
      <!--begin::Page Scripts -->
      <script src="assets/admin/assets/app/js/dashboard.js" type="text/javascript"></script>
      <!--end::Page Scripts -->
   </body>
   <!-- end::Body -->
</html>