
@extends('register_app') 
@section('content')
    <!-- end::Head -->

    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
	<script src="{{ asset('js/app.js') }}"></script>
    <!-- begin::Body -->
    <body  class="m--skin- m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--fixed m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default"  >

        
        
    	<!-- begin:: Page -->
<div class="m-grid m-grid--hor m-grid--root m-page">
    
			
				<div class="m-grid__item m-grid__item--fluid m-grid m-grid--hor m-login m-login--signin m-login--2 m-login-2--skin-3" id="m_login" style="background-image: url(./assets/admin/assets/app/media/img//bg/bg-2.jpg);">
	<div class="m-grid__item m-grid__item--fluid	m-login__wrapper">
		<div class="m-login__container">
			<div class="m-login__logo">
				<a href="#">
				<img src="./assets/admin/assets/app/media/img/logos/logo-5.png">  	
				</a>
			</div>
			<div class="m-login__signin">
				<div class="m-login__head">
					<h3 class="m-login__title">Sign In</h3>
				</div>
				<form class="m-login__form m-form form-horizontal" action="{{ route('login') }}">
				     {{ csrf_field() }}
					<div class="form-group m-form__group">
						<input class="form-control m-input"   type="text" placeholder="Email" name="email" autocomplete="off">
					</div>
					<div class="form-group m-form__group">
						<input class="form-control m-input m-login__form-input--last" type="password" placeholder="Password" name="password">
					</div>
					<div class="row m-login__form-sub">
						<div class="col m--align-left m-login__form-left">
							<label class="m-checkbox  m-checkbox--light">
							<input type="checkbox" name="remember"> Remember me
							<span></span>
							</label>
						</div>
						<div class="col m--align-right m-login__form-right">
							<a href="javascript:;" id="m_login_forget_password" class="m-link">Forget Password ?</a>
						</div>
					</div>
					<div class="m-login__form-action">
						<button id="m_login_signin_submit" type="submit" class="btn btn-focus m-btn m-btn--pill m-btn--custom m-btn--air  m-login__btn">Sign In</button>
					</div>
				</form>
			</div>
		
		</div>	
	</div>
</div>				
		

</div>
<!-- end:: Page -->


        <!--begin::Global Theme Bundle -->
                    <script src="./assets/admin/assets/vendors/base/vendors.bundle.js" type="text/javascript"></script>
                    <script src="./assets/admin/assets/demo/default/base/scripts.bundle.js" type="text/javascript"></script>
                <!--end::Global Theme Bundle -->

        
                    
        
                    </body>
    <!-- end::Body -->
</html>