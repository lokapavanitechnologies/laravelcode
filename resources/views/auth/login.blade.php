@extends('layouts.app')

@section('content')
<div class="m-grid m-grid--hor m-grid--root m-page">
<div class="m-grid__item m-grid__item--fluid m-grid m-grid--hor m-login m-login--signin m-login--2 m-login-2--skin-3" id="m_login" style="background-image: url(./assets/admin/assets/app/media/img//bg/bg-2.jpg);">
	<div class="m-grid__item m-grid__item--fluid	m-login__wrapper">
<div class="m-login__container">
		    <div class="m-login__logo">
				<a href="#">
				<img src="./assets/admin/assets/app/media/img/logos/logo-5.png">  	
				</a>
			</div>
            <div class="m-login__signin">
                <div class="m-login__head">
					<h3 class="m-login__title">Sign In</h3>
				</div>
                 
                
                    <form class="m-login__form m-form form-horizontal" method="POST" action="{{ route('login') }}">
                        {{ csrf_field() }}

                        <div class="form-group m-form__group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-6 control-label">E-Mail Address</label>

                            <div class="col-md-12">
                                <input id="email" type="email" class="form-control m-input" name="email" value="{{ old('email') }}" required autofocus>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group m-form__group {{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-6 control-label">Password</label>

                            <div class="col-md-12">
                                <input id="password" type="password" class="form-control m-input" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="row m-login__form-sub">
						<div class="col m--align-left m-login__form-left">
							<label class="m-checkbox  m-checkbox--light">
							<input type="checkbox" name="remember"> Remember me
							<span></span>
							</label>
						</div>
						<div class="col m--align-right m-login__form-right">
							<a href="{{ route('password.request') }}" id="m_login_forget_password" class="m-link">Forget Password ?</a>
						</div>
					</div>

                        <div class="form-group">
                            <div class="col-md-12 col-md-offset-4">
                                <button type="submit" id="m_login_signin_submit" class="btn btn-primary">
                                    Login
                                </button>
                            </div>
                        </div>
                    </form>
               
            </div>
        
   
</div>
</div>
</div>
@endsection
