
<!doctype html>
<html>
<head>
    @include('layout.headerscripts')
</head>
<body>
<div class="container">

    <div id="main" class="row">

            @yield('content')

    </div>

   
</div>
</body>
</html>