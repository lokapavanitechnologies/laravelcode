<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth; 
use DB;
use Illuminate\Support\Facades\Mail;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Request $request)
    {
        $this->middleware('auth');
		$this->request=$request;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
		if(Auth::user()->role == 1)
        {	
        return view('admin/dashboard');
		}
		else {
		return view('user/profile');	
		}
    }
	
	/**
     * Show the user register page.
     *
     * @return \Illuminate\Http\Response
     */
    public function add_user()
    {
        return view('admin/users');
    }
	
	/**
     * Save the user register data.
     *
     * @return \Illuminate\Http\Response
     */
    public function save_user(Request $request)
    {
		$post_data = $request->input();
		$email=$post_data['email'];
		$existmail=DB::table('users')->where('email',$email)->orWhere('name', 'like', '%' . $email . '%')->count(); 
		if($existmail == 0) {
		$password=str_random(8);
		$savePath = "uploads/profiles/";
        $extension = $request->file('userfile')->getClientOriginalExtension();
        $fileName =  time().'.'.$extension;
		$request->file('userfile')->move($savePath, $fileName);
        $user = array(
		'name' => $post_data['firstname'],
		'firstname' => $post_data['firstname'],
		'lastname' => $post_data['lastname'],
		'pmname' => $post_data['pmname'],
		'role' => $post_data['role'],
		'dob' => date('Y-m-d', strtotime(str_replace('-', '/', $post_data['dob']))),
		'gender' => $post_data['gender'],
		'email' => $post_data['email'],
		'phone_no' => $post_data['phone_no'],
		'password' => bcrypt($password),
		'address' => $post_data['address'],
		'city' => $post_data['city'],
		'state' => $post_data['state'],
		'zipcode' => $post_data['zipcode'],
		'social' => $post_data['social'],
		'img'  => $fileName,
		'created_at' => date('Y-m-d H:i:s')
	   );
		DB::table('users')->insert($user);
		
		Mail::raw('Hi, welcome user! ', function ($message) use($email,$password){
   $message->setBody( "<html><h1>Your Login details</h1><p>username : $email</p><p>password : $password</p><p>Thanks for Register.</p></html>", 'text/html' );			
  $message->from('sai.ashok58@gmail.com', 'Dms');			
  $message->to($email)
    ->subject("Login details");
});
		/*  if (count($result) > 0) {
            Mail::send('emails.businessUsersReport', $user, function ($message)  {
                $message->from('sai.ashok58@gmail.com', 'Logistiks Report');
                $message->to('sai.ashok58@gmail.com');
                
					$message->subject("Logistiks new users and transactions report for the date ");
				
            });
        } */
		
		return redirect('userlist');
		
		}
		else {
			
		return redirect()->back()->with('message', 'Email Already Existed');	
		}
		
    }
	
	
	/**
     * Show the all user list.
     *
     * @return \Illuminate\Http\Response
     */
    public function userlist()
    {
		$data['users']=DB::table('users')->get(); 
        return view('admin/userslist',$data);
    }
	
	
	/**
     * Show user profile view.
     *
     * @return \Illuminate\Http\Response
     */
    public function userprofile($id)
    {
		$data['users']=DB::table('users')->where('id',$id)->first(); 
        return view('admin/profile',$data);
    }
	
	
	/**
     * delete the user.
     *
     * @return \Illuminate\Http\Response
     */
    public function userprofiledelete($id)
    {
		$data['users']=DB::table('users')->where('id',$id)->delete(); 
        return redirect()->back()->with('message', 'User Deleted Successfully');	
    }
	
	/**
     * Save the user register data.
     *
     * @return \Illuminate\Http\Response
     */
    public function updateprofile($id)
    {
		$post_data = $this->request->all();
		$savePath = "uploads/profiles/";
        $extension = $this->request->file('userfile')->getClientOriginalExtension();
        $fileName =  time().'.'.$extension;
		$this->request->file('userfile')->move($savePath, $fileName);
        $user = array(
		'name' => $post_data['firstname'],
		'firstname' => $post_data['firstname'],
		'lastname' => $post_data['lastname'],
		'phone_no' => $post_data['phone_no'],
		'address' => $post_data['address'],
		'city' => $post_data['city'],
		'state' => $post_data['state'],
		'zipcode' => $post_data['zipcode'],
		'social' => $post_data['social'],
		'img' => $fileName,
		'updated_at' => date('Y-m-d H:i:s')
	   );
		DB::table('users')->where('id' , $id)->update($user);
		
		
		return redirect(userlist);
		
    }
	
	/**
     * Show add policy .
     *
     * @return \Illuminate\Http\Response
     */
    public function addpolicy()
    {
		$data['users']=""; 
        return view('admin/policy',$data);
    }
	
	
	/**
     * Show add policy .
     *
     * @return \Illuminate\Http\Response
     */
    public function policy()
    {
		$data['users']=""; 
        return view('admin/policy_view',$data);
    }
	
	
	
	
	
	
}
