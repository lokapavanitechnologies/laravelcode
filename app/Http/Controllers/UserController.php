<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth; 
use DB;
use Illuminate\Support\Facades\Mail;
class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the user profile view.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
		$auth_id=Auth::user()->id;
		$data['users']=DB::table('users')->where('id',$auth_id)->first();
        return view('user/profile',$data);
    }
	
}
