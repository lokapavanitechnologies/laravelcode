
$(document).ready(function () {
			
			$('#m_form_add').validate({
				errorClass: "error-1",
				
				ignore: ':not(select:hidden, input:visible, textarea:visible):hidden:not(#is_decleration_accept)',
				

				rules: {
					fastname:{
						required: true,
					},
					lastname:{
						required: true,
					},
					phone_no: {
						required: true,
						mobileNumber: true,
						maxlength: 10
					},
					 business_name : {
			          	required : true
			          },  
			           business_place : {
			          	required : true
			          }, 
			         business_designatn: {
			         	required : true
			         }, 
			         business_const : {
			         	required : true
			         }, 
					pincodeLocation: {
						required: true,
						pincodeValidate:true
					},
					business_location: {
						required: true,
						//pincode:true
					},
					business_city: {
						required: true,
						//pincode:true
					},
					business_district: {
						required: true,
						//pincode:true
					},
					business_state: {
						required: true,
						//pincode:true
					},
					address1: {
						required: true,

					},
					address2: {
						required: true,

					},
					year_of_est: {
						required: true,

					},

					business_emailId: {
						required: true,
						email:true

					},


					business_type_id: {
						required: true

					},
					industry_type_name: {
		                required: true,
		                
		            },
		             sector_type: {
		                required: true,
		                
		            },
					business_pan: {
						required :true,
						panCard:true,
						/*remote:{
						 url: '/validatePancard?',
						 type: "post"
						 }*/

					},

					ifsc_code: {
						required :true,
						ifscCode : true

					} ,
					
					tin_no: {
						tinNumber: true

					},

					cin_no: {
						required :true,
						CinNumber: true

					},
					current_turnover: {
						required :true,
						twoDecimalPlace: true
						
						//turnovervalidations: true
					},


					first_year_turnover: {
                        required :true,
						twoDecimalPlace: true
					},
					second_year_turnover: {

						twoDecimalPlace: true
					},
					third_year_turnover: {

						twoDecimalPlace: true
					},
					cdbaccept: {
						required: true
					},
					sellertdsaccept: {
						required: true
					},
					contact_fname: {
						required: true
					},

					contact_lname: {
						required: true
					},

					employee_strn: {
						required: true
					},
					 bank_name : {
			         	required : true
			         },
			         bank_branch : {
			         	required : true
			         },
			         account_type : {
			         	required : true
			         }, 
			    
				},
				 messages: {
					account_no: {
						minlength: "Please enter atleast 9 digits",
						maxlength: "Please enter not more than 17 digits"
					}
				},
				errorPlacement: function(error, element) {
					var parentTag = $(element).parent();
					if(parentTag.is('span')) {
						$(element).parent('span').after(error);
					} else {
						$(element).parent('div').after(error);
					}

				},


			});
});