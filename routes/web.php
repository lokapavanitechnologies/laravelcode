<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth/login');
});


Route::post('/dashboard', 'HomeController@dashboard');
Route::get('/dashboard', 'HomeController@dashboard');


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/user', 'UserController@index')->name('user');
Route::post('/adduser', 'HomeController@add_user')->name('adduser');
Route::get('/adduser', 'HomeController@add_user')->name('adduser');
Route::post('/saveuser', 'HomeController@save_user')->name('saveuser');
Route::post('/userlist', 'HomeController@userlist')->name('userlist');
Route::get('/userlist', 'HomeController@userlist')->name('userlist');
Route::get('/saveuser', 'HomeController@save_user')->name('saveuser');
Route::any('/userprofiledelete/{id}', 'HomeController@userprofiledelete');
Route::any('/userprofile/{id}', 'HomeController@userprofile');
Route::post('updateprofile/{id}', 'HomeController@updateprofile');
Route::any('addpolicy', 'HomeController@addpolicy');
Route::any('policy', 'HomeController@policy');

